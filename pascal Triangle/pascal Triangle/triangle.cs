﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pascal_Triangle
{
    class triangle
    {
        private int centre(int x, int y)
        {
            if (y == 1 || (x > 0 && y == x)) 
                return 1;
            else 
                return (centre(x - 1, y) + centre(x - 1, y - 1));
        }
        public triangle()
        {
            Console.WriteLine("Creating Pascal Triangle");
        }
       

        public void pascaltriangle(int n)
        {
            for (int i = 1; i <= n; i++)
            {
                int numbers = n - i;
                for (int j = 1; j <= numbers ; j++)
                {
                    Console.Write(" ");
                }
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(" {0}", centre(i, j));
                }
                Console.WriteLine();
            }
        }
   }
}