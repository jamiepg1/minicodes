/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author malikali
 */
public class Main {

    /**
     * @param args the command line arguments
     */

    private static int serverPort ;
    private static String serverIP;
    private static String inputFolder = "input";
    private static String configFile = "myconfig";
    private static String inputFIle = "input.txt";
    private static String outputFolder = "output";
    private static String outputFile = "output.txt";
    public static void main(String[] args)
    {
        try {

            configure();


            //Create Socket on Any Available PORT
            DatagramSocket ds =  new DatagramSocket(0);
            //ds.setSoTimeout(1000);

            //Check and Create Output Directory
            File outfolder = new File(outputFolder);
            if(!outfolder.exists())
                outfolder.mkdir();


            File outFile = new File(outputFolder+File.separatorChar+outputFile);
            if(!outFile.exists())
            {
                outFile.createNewFile();
            }
            FileOutputStream out= new FileOutputStream( outFile);
            PrintStream p = new PrintStream(out) ; // declare a print stream object



            //Open File for Server Query

            File fil = new File(inputFolder + File.separatorChar + inputFIle);

            InputStreamReader is;
            
            is = new FileReader(fil);
            BufferedReader bf = new BufferedReader(is);
            String s;
            while( (s = bf.readLine())!=null )
            {


                byte[] requestBytes = s.getBytes();
                //Create Datagram Packet for sending
                DatagramPacket dp = new DatagramPacket(requestBytes, requestBytes.length, InetAddress.getByName(serverIP), serverPort);
                DatagramPacket in = null;
                while(true)
                {
                    ds.send(dp);
                    byte[] response = new byte[1024];
                    in = new DatagramPacket(response , response.length);
                    try
                    {
                        ds.receive(in);
                    }
                    catch(SocketException ex)
                    {
                        continue;
                    }
                    break;
                }
                String IP = new String(in.getData());
                Thread.sleep(2000);
                p.println(IP);
            }
            ds.close();
            p.close();
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } 

    }

     private static void configure()
    {
        try
        {

            File fil = new File(inputFolder + File.separatorChar + configFile);
            InputStreamReader is;
            is = new FileReader(fil);
            BufferedReader bf = new BufferedReader(is);
            String s;
            s = bf.readLine();
            String[] split = s.split(" ");
            serverIP = split[0];
            serverPort = Integer.parseInt(split[1]);
            bf.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

