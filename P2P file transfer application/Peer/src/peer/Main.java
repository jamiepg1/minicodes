/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package peer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Hashtable;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import objectsender.Request;

/**
 *
 * @author malikali
 */
public class Main extends Thread {


    public static String myIP;
    public static String neibourIP;
    public static int myPort;
    public static int neibourPort;
    private static Hashtable<String , String> hashtable = new <String , String> Hashtable();
    private static String Inputfolder = "input";
    private static String inputFileName  = "input.in";
    private static String configrationfile = "myconfig";
    /**
     * @param args the command line arguments
     * @throws IOException
     */
    public static void main(String[] args) throws IOException
    {
        Initialise();
        
        Main obj = new Main();

        obj.start();

        obj.go();
    
    }
    public static void print(Object s)
    {
        System.out.println(s.toString());
    }


    private static void Initialise()
    {
        File fil = new File(Inputfolder+File.separatorChar+configrationfile);

        InputStreamReader is;
        try {

                is = new FileReader(fil);

                BufferedReader bf = new BufferedReader(is );
                String s ;
                s = bf.readLine();
                String []split = s.split(" ");
                myIP = split[0];
                myPort = Integer.parseInt(split[1]);

                s = bf.readLine();
                split = s.split(" ");
                neibourIP = split[0];
                neibourPort = Integer.parseInt(split[1]);
                File input = new File(Inputfolder);
                if (!input.exists())
                input.mkdir();
                isDirectory(Inputfolder);
        }
        catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static boolean isDirectory(String name)
    {
        
        File f = new File(name);
        String [] s = f.list();
        for(int i = 0  ; i < s.length ; i++)
        {
            
            String Name = name+"/"+s[i];
            File fil = new File(Name);
            if(fil.isDirectory())
                isDirectory(Name);
            else
            {
                String[] spli = Name.split("/");
                print(Name);
                hashtable.put(spli[spli.length-1], Name);
                
            }
        }
         return f.isDirectory();

    }
    private void go()
    {
            ServerSocket ss;
        try {
                print("waiting for connection at :"+myPort);
                ss = new ServerSocket(myPort);

                while(true)
                {
                    Socket s = ss.accept();
                    print("I got a Connection");
                    ObjectInputStream ois = new ObjectInputStream (s.getInputStream());

                    Request r = (Request)ois.readObject();
                    if(r == null)
                        continue ;
                    
                    if(r.isFound)
                    {
                        print("File found on a Peer Requesting File Transfer");
                       RequestFile obj = new RequestFile(r);
                       obj.start();
                       continue;
                    }

                    if(myIP.equals( r.IP) && myPort == r.portNUMBER)
                    {
                        print("File Not Found");
                         continue;
                    }
                    
                    if (!r.isFound)
                    {
                        if (hashtable.containsKey(r.fileName))
                        {
                            print("FIle Found");
                             
                            FT obj = new FT(r, hashtable.get(r.fileName));
                            obj.start();
                        }
                        else
                        {

                            print("File not found in my Directory");
                         
                            Socket soc = new Socket(neibourIP , neibourPort);
                            
                            ObjectOutputStream oos = new ObjectOutputStream(soc.getOutputStream());
                            oos.writeObject(r);
                            oos.flush();
                            oos.close();
                            s.close();
                        }

                    }
                    

                }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }


    }
    @Override
    @SuppressWarnings("SleepWhileHoldingLock")
    public void run()
    {
        try {

            File file = new File(Inputfolder+File.separatorChar+inputFileName);
            InputStreamReader in = new FileReader(file);
            BufferedReader br = new BufferedReader(in);

            //BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
              String name = null;

                  while((name = br.readLine())!=null)
                  {

                       // System.out.print("$");



                        Request res = new Request(myPort, myIP, name, false);
                        

                        while(true)
                        {
                            try
                            {
                                Socket s = new Socket(neibourIP , neibourPort);
                                ObjectOutputStream obj = new ObjectOutputStream(s.getOutputStream());
                                obj.writeObject(res);
                                obj.flush();
                                obj.close();
                                s.close();
                                break;
                            }
                            catch(IOException ex)
                            {
                                Thread.sleep(100);
                                continue;
                            }
                            
                       }
                        // FOr random Delay in file Request
                        Thread.sleep((long) Math.abs( 3000 * Math.random())); 
                        
                        
                 }
            }
        catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }            catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            
    }

}
