TITLE 

; Write your comments here:
; 
; 

INCLUDE Irvine32.inc

.data
msg byte 'enter integer ',0
fail byte 'Drop the Course',0
improve byte 'Work Hard',0
cong byte 'Congratulation ! Good performance',0

.code
main PROC
;code comes here 
mov edx , offset msg
call writestring
call readint
mov ebx , eax
call writestring
call readint
add ebx , eax
cmp ebx , 59
jle f
cmp ebx , 69
jle d
cmp ebx , 79
jle ccc
cmp ebx , 89
jle b

mov al, 0010b
call SetTextColor
mov al , 'A'
call writechar
call Crlf
mov edx , offset cong
call writestring

jmp quit


b:
mov al, 0010b
call SetTextColor
mov al , 'b'
call writechar
call Crlf
mov edx , offset cong
call writestring

jmp quit


ccc:
mov al, 1110b
call SetTextColor
mov al , 'c'
call writechar
call Crlf
mov edx , offset improve
call writestring

jmp quit

D:
mov al, 1110b
call SetTextColor
mov al , 'd'
call writechar
call Crlf
mov edx , offset improve
call writestring

jmp quit
f:
mov al , 0100b
call SetTextColor
mov al , 'f'
call writechar
call Crlf
mov edx , offset fail
call writestring

quit:
;code ends here
	;call DumpRegs	; display the registers
	exit
main ENDP
END main