TITLE 
; Assingment 2 Question Number 7
; Mansoor Ali	 
; K080097
; Objective : Solving an Equation 

INCLUDE Irvine32.inc

.data
valx SDWORD 10
valy SDWORD -13
valz SDWORD 20
.code
main PROC
; Expresion : EAX = -valy + 7 - valz + valx or EAX = 7+valx-valy-valz
mov eax , 7    ; Moving 7 in Eax  EAX =7
add eax , valx ; adding valx in eax eax = 11

sub eax , valy ; Subtracting valy from eax = 1E 
call DumpRegs
sub eax , valz ; Subtracting valz from eax = A


	call DumpRegs	; display the registers
	exit
main ENDP
END main