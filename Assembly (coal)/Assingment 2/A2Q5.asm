TITLE 

; Assingment 2 Question Number 5
; Mansoor Ali	 
; K080097
; Objctive : To calculate the grade by taking Number as input
INCLUDE Irvine32.inc

.data


.code

main PROC
S:  ; defined to check for rest
CALL READINT  ; wait 4 the user to input value and it is stored in eax

cmp ax , 100 ; if number is greater than 100 then take input again 
jg S
cmp ax , 0   ; if number is less than 0 then take input again
jl S

cmp ax , 90   ; compare the input value with 90 
JGE gradeA    ; jump to label gradeA if greaterthan or eqaual to	
cmp ax , 80
JGE gradeB
cmp ax , 70
JGE gradeC
cmp ax , 60
JGE graded

jmp gradeF

gradeA: 
mov ax , 'A'   ; moving ascii A in ax register to print 
call WriteChar ; converts the value in ascii and print it on screen
jmp en	       ;jump to the en label else it will print all the carachters to the end 	
gradeB:
mov ax , 'B'
call WriteChar
jmp en
gradeC:
mov ax , 'C'
call WriteCHAR
jmp en
gradeD:
mov ax , 'D'
call WriteCHAR
jmp en  
gradeF:
mov ax , 'F'
call WriteCHAR 


en:  ; for ending the program

;call DumpRegs	; display the registers
	exit
main ENDP
END main