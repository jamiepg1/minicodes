TITLE 

; Mansoor Ali Malik
; Coal Lab 8 Task 4
; K080097 

INCLUDE Irvine32.inc

.data
inp byte 5 dup(0)
msg byte 'enter number ' , 0
temp1 byte ?
temp2 byte ?
.code
main PROC
mov esi , offset inp
mov ecx , 5

q:
	mov edx , offset msg		
	call writestring		
	call readint
	mov [esi] , al
	inc esi
	loop q



mov ecx , 1
mov esi , offset inp
j:
    mov edx , offset inp
    add edx , ecx ; to get the next position of current position in esi
    mov eax , 0   ; for five integers 
    add eax , ecx ; to calculate how much values to b calculated
    k:
	mov bl , [edx]
  	mov temp1 , bl
	mov bl , [esi]   
	.if( bl > temp1)
                push eax ; USING THESE FOR SWAPING
		push ecx 
		mov eax , [esi]
		mov ecx , [edx]
		mov [edx] , al
		mov [esi] , cl
		pop ecx
		pop eax
	.endif
	   inc eax
	   inc edx
	cmp eax , 5
	jne k
    inc esi
    inc ecx
    cmp ecx , 5
    jne j

mov esi , offset inp
mov ecx , 5

g:
	mov al , [esi]
	call writeint
	inc esi
	loop g


;call DumpRegs	; display the registers
	exit
main ENDP
END main