TITLE 


; Mansoor Ali
; K080097
; Lab 7 Task5

INCLUDE Irvine32.inc

.data
msg byte "enter character to u want to print",0
.code
main PROC

call clrscr
mov edx,offset msg
call writestring
call readchar
mov ebx , eax     
mov ecx , 100 ; for printing 100 characters
call clrscr
j:
     
     mov eax , ecx
     call settextcolor ; for diffent colors 
     mov eax , 15 ; random value range
     call Randomrange 
     
     mov dh , al ; y-axis ; storing first eight bits of eax to dh
     mov eax , 35
     call Randomrange ; for second random value:
     mov dl , al ; x-axis	   
     call gotoxy
     mov eax , ebx
     call writechar     
loop j
; moving cursor to the end of command prompt
mov dh , 30
mov dl , 0 
call gotoxy 
;code ends here
	;call DumpRegs	; display the registers
	exit
main ENDP
END main