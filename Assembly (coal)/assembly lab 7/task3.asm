TITLE 


; Mansoor Ali
; K080097
; Lab 7 Task3

INCLUDE Irvine32.inc

.data

.code
main PROC

   call clrscr ; for clearing the screen
   mov dh , 10 ; y-axis
   mov dl , 35 ; x-axis	   
   call gotoxy
   call readint  ; read integer
   mov ebx , eax ; moving value in other register
   mov dh , 11 ; y-axis ; adding in y axis for next next line
   mov dl , 35 ; x-axis
   call gotoxy	   
   call readint ; reading the 2nd value
   add eax , ebx;adding the previous value in the currtly read value
   mov dh , 12 ; y-axis
   mov dl , 35 ; x-axis
   call gotoxy	; moving on the cursor to the next line
   call writeint
e:
;code ends here
	;call DumpRegs	; display the registers
	exit
main ENDP
END main