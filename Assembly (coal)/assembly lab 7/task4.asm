TITLE 


; Mansoor Ali
; K080097
; Lab 7 Task4 

INCLUDE Irvine32.inc

.data

.code
main PROC


mov ecx , 50 ; for looping 

j:
     mov eax , 40 ; seting range for random value
     call Randomrange ; calling function for the value
     sub eax , 20  ;subtracting the value to generate negative numbers
     call writeint ;printing random number 
     mov eax , 10 ; for next line
     call writechar	   
loop j ; dec ecx and cmp ecx with 0
;code ends here
	;call DumpRegs	; display the registers
	exit
main ENDP
END main