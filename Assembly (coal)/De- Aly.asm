TITLE DE-ALY
;AssemblY Final Project


;	K08-0075
;	K08-0097
;	k08-0188


INCLUDE Irvine32.inc

.data
op1 byte '0' ; For storing 1st operator of an instruction

op2 byte '0' ;For Storing 2nd operator of an instruction

buffer byte  500 dup(?) ; For Reading File 

buffersize = ($-buffer) ; basically its length but books is showing it as size

MYSTR byte 30 dup(?)

errMSg Byte "cannot Open the File ",0dh , 0ah , 0

filename Byte "in.cpp",0

filehandle Dword ?

bytecount Dword ?

lengthofinput byte 0

er byte 'Error' , 0

leng byte ?

.code

;--------------------------------------Codes Starts From Here ----------------


;-----------------------------------------------------------------------------
;------------------------ Clearing the Instrcution ---------------------------
;-----------------------------------------------------------------------------

clr PROC
PUSHAD  	  
  movzx ecx , lengthofinput ; moving length of the current instruction in ecx
  mov esi ,offset mystr ; moving initial address of current instruction string
  mov eax , ' '
  q:
	mov [esi] , eax ; moving space in the instruction
	inc esi
	loop q

Call CRLF

POPAD

ret
clr ENDP


;-----------------------------------------------------------------------------
;----------------------------Equalto------------------------------------------
;-----------------------------------------------------------------------------

assin proc
.data
assinment byte "mov al , " ,0dh , 0ah,"mov , " , "al",0dh , 0ah,0
.code
pushad
mov ecx, 9 ; moving 9 to print the assinment byte till "mov al , "
mov esi , offset assinment
mov leng,1
p:
	mov eax , [esi]
	call writechar
	inc esi
	loop p 

mov esi , offset mystr ; Moving Initioal address of current instrution in esi
movzx ecx , lengthofinput ; moving length of current instruction in ecx 
q:
	mov eax , [esi] ; moving current character in eax
	inc esi	
	dec ecx	;decrementing the ecx 
	inc leng
	cmp al , '='	; comparing the current character in string with = 
	jne q

r:
	mov eax , [esi] ; printing the characters after the = character 
	call writechar
	inc esi
	loop r


	mov esi , offset assinment ; moving the offset of the assingment 
	add esi , 9 ; v have already printed till mov al ,
	
	mov ecx , 6 ; moving 6 to print till mov
	t:
	mov eax , [esi] ; moving character to print on screen
	call writechar	; printing on screen
	inc esi
	loop t
	
	mov esi , offset mystr	;moving the offset of current character 
	u:
	mov eax , [esi] ; moving the character to print
	call writechar	
	inc esi	; to get the next character 
	mov ebx , [esi]	; moving the character of checking
	cmp bl , '='	; cheacking if the character is = or not
	jne u	; looping till we get the = 

mov esi , offset assinment ; moving the offset of the assingment 
add esi , 14 ; already printed till mov 
mov ecx , 6  ; for printing the next part , al
	v:
	mov eax , [esi]
	inc esi
	call writechar
	loop v		
popad
ret
assin endp

;---------------------------Post FIX Instructions ----------------------------

;-----------------------------------------------------------------------------
;------------------------------- Increment -----------------------------------
;-----------------------------------------------------------------------------

increment proc
.data
incremen byte "inc ",0
.code
pushad

mov edx , offset incremen
call writestring ; Print Whole instruction
mov esi , offset mystr
a:
	mov eax , [esi]
	inc esi
	call writechar
	mov ebx , [esi]
	cmp bl , '+' ; Printing till the + sign 
	jne a ; jmp to a if current character is not equal to +

popad
ret
increment endp

;-----------------------------------------------------------------------------
;----------------------------- Decrement -------------------------------------
;-----------------------------------------------------------------------------

decre proc
.data
decrem byte 'dec ',0
.code
pushad
mov edx , offset decrem 
call writestring ; Print Whole instruction
mov esi , offset mystr
a:
	mov eax , [esi]
	inc esi
	mov [ebx],eax
	inc ebx
	call writechar
	mov ecx , [esi]
	cmp cl , '-' ; Printing till the - sign 
	jne a ;jmp to a if current character is not equal to -
popad
ret
decre endp

;-------------------------Arithmetic operations ------------------------------

;-----------------------------------------------------------------------------
;----------------------------- Division PROC ---------------------------------
;-----------------------------------------------------------------------------

did proc   
.data
division byte "movzx ax , mov bl ,div blmov  , al",0 
.code
pushad
mov leng , 0

 mov esi , offset division  ;moving offset of division
 mov ecx , 10	; Print till ax ,  
 p:
	mov eax , [esi]
	call writechar
	inc esi

	loop p 

mov esi , offset mystr 

q:
	inc leng ; want to store the number of characters before =
	inc esi
	mov eax , [esi]
	cmp al , '=' ; mov esi till = 
	jne q
inc esi	; Skip =

r:
	mov  eax, [esi] ; = is skiped to printing after that
	call writechar	
	inc esi	
	inc leng   ; want to store the number of characters before /
	mov ebx , [esi]
	cmp bl , '/' ; print till / it can b variable or constant value
	jne r


inc leng ; increment to get position of /
inc leng ; to skip /

call crlf

mov esi , offset division
add esi , 11 ;skiping previously used data
mov ecx , 8 ; now printing till mov bl ,
e:
	mov eax , [esi]
	call writechar
	inc esi
	loop e

mov esi , offset mystr
 
movzx eax , leng 

add esi , eax ; geting the pointer to the next character of /

mov cl , lengthofinput ; moving length of instruction

sub cl , leng ; geting length of last variable

t:
	mov eax , [esi]
	inc esi
	call writechar
	loop t

call crlf

mov esi , offset division

add esi , 19  ; Skiping Previous data

mov ecx , 6   ; printing div bl

o:
	mov eax , [esi]
	call writechar
	inc esi
	loop o
call crlf
mov ecx , 4 ; now printing mov 
w:
	mov eax , [esi]
	call writechar
	inc esi
	loop w
mov esi , offset mystr

m:
	mov ax , [esi]
	inc esi
	call writechar ; printing the 1st variable which is till =
	mov bx , [esi]
	cmp bl , '='	
	jne m	; comparing the current character with = and loop till we find = 
mov esi , offset division

add esi , 29 
mov ecx , 5	; printing , al 

f:
	mov eax , [esi]
	call writechar
	inc esi
	loop f
popad
ret
did endp

;-----------------------------------------------------------------------------
;----------------------------Divide Equal ------------------------------------
;-----------------------------------------------------------------------------

dive proc 
.data
deq byte "movzx ax , mov bl ,div blmov  , al",0 
.code
pushad
mov leng , 0

 mov ecx , 10 ; want to print till , 
 mov esi , offset deq
 
 a:
	mov eax , [esi]
	call writechar 
	inc esi

	loop a ; printing till al ,

mov esi , offset mystr

q:
	mov eax , [esi]
	call writechar
	inc esi
	inc leng
	mov ebx , [esi]	
	cmp bl , '/' ; printing till the divide sign
	jne q
inc leng ; to skip /
inc leng ; to skip =
call crlf

mov esi , offset deq
add esi , 11 ; adding 11 because we have already printed till al , 
mov ecx , 8  ; want to print till bl,
e:
	mov eax , [esi]
	call writechar
	inc esi
	loop e

mov esi , offset mystr
movzx eax , leng ; leng is calculated position of = in current instruction 
add esi , eax  ; adding leng in esi
mov cl , lengthofinput ; moing length of instuction
sub cl , leng	; Subtracting value till = to print the remaining part that is after /=

t:
	mov eax , [esi]
	inc esi
	call writechar
	loop t
call crlf
mov esi , offset deq
add esi , 19  ; printed till bl,
mov ecx , 6 ; now printing till second bl of div
o:
	mov eax , [esi]
	call writechar
	inc esi
	loop o
call crlf
mov ecx , 4  ;mov printing mov
w:
	mov eax , [esi]
	call writechar
	inc esi
	loop w
mov esi , offset mystr

m:
	mov ax , [esi]
	inc esi
	call writechar
	mov bx , [esi]
	cmp bl , '/' ; printing till /
	jne m
mov esi , offset deq

add esi , 29 ; allready printed till mov
mov ecx , 5	; printing end of instruction

f:
	mov eax , [esi]
	call writechar
	inc esi
	loop f
popad
ret
dive endp
;-----------------------------------------------------------------------------
;----------------------------------Mod ---------------------------------------
;-----------------------------------------------------------------------------
remain proc
.data
modu byte "movzx ax , mov bl ,div blmov  , ah",0 
.code 
pushad	
 mov leng , 0
 mov ecx , 10 ; printing till ax , 
 mov esi , offset modu
 
 p:
	mov eax , [esi]
	call writechar
	inc esi

	loop p ; printing till al ,

mov esi , offset mystr

q:
	inc leng
	inc esi
	mov eax , [esi]
	cmp al , '=' ; moving the offset of esi till =
	jne q
inc esi ; skiping =

r:
	mov  eax, [esi]
	call writechar
	inc esi
	inc leng
	mov ebx , [esi]
	cmp bl , '%' ; printing till the mod sign
	jne r
inc leng ; leng is till the previous byte of % to skip that
inc leng ; to skip %
call crlf

mov esi , offset modu
add esi , 11 ; Printed till ax ,
mov ecx , 8  ;Print till bl ,
s:
	mov eax , [esi]
	call writechar
	inc esi
	loop s

mov esi , offset mystr 
movzx eax , leng ; Moving position till % sign
add esi , eax	 ; ADding position in the offset 
mov cl , lengthofinput ; length of input
sub cl , leng	; to get the length from % to last character

t:
	mov eax , [esi]
	inc esi
	call writechar
	loop t
call crlf
mov esi , offset modu
add esi , 19  ; printed till bl,
mov eax , [esi]
mov ecx , 6	;now till div bl,
o:
	mov eax , [esi]
	call writechar
	inc esi
	loop o
call crlf
mov ecx , 4 ; now to print mov	
w:
	mov eax , [esi]
	call writechar
	inc esi
	loop w
mov esi , offset mystr

m:
	mov ax , [esi]
	inc esi
	call writechar
	mov bx , [esi]
	cmp bl , '=' ; printing the 1st variable which is before =
	jne m
mov esi , offset modu

add esi , 29  
mov ecx , 5 ; printing the remaining part

f:
	mov eax , [esi]
	call writechar
	inc esi
	loop f
popad
ret
remain endp

;-----------------------------------------------------------------------------
;----------------------------------Mod Equal----------------------------------
;-----------------------------------------------------------------------------

modequa proc 
.data
modeq byte "movzx ax , mov bl ,div blmov  , ah",0
.code
pushad
mov leng , 0

 mov ecx , 10
 mov esi , offset modeq ;Printin till ax ,
 
 p:
	mov eax , [esi]
	call writechar
	inc esi

	loop p ; printing till al ,

mov esi , offset mystr

q:
	mov eax , [esi]
	call writechar
	inc esi
	inc leng
	mov ebx , [esi]	
	cmp bl , '%' ; printing till the % 
	jne q
inc leng ;skip %
inc leng ; to skip the = sign
call crlf

mov esi , offset modeq
add esi , 11 ; skiping the printed data 
mov ecx , 8 ; Printing mov bl ,
e:
	mov eax , [esi]
	call writechar
	inc esi
	loop e

mov esi , offset mystr
movzx eax , leng
add esi , eax ; adding position of = because the operator is %= and v want to print after that 
mov cl , lengthofinput
sub cl , leng ; to print characters after %=

t:
	mov eax , [esi] ; printing the characters after %=
	inc esi 
	call writechar 
	loop t
call crlf
mov esi , offset modeq 
add esi , 19  ; skiping the previous printed data 
mov eax , [esi]
mov ecx , 6 ;print div bl
o:
	mov eax , [esi]
	call writechar
	inc esi
	loop o
call crlf
mov ecx , 4 ;print mov 
w:
	mov eax , [esi]
	call writechar
	inc esi
	loop w
mov esi , offset mystr

m:
	mov ax , [esi]
	inc esi
	call writechar
	mov bx , [esi]
	cmp bl , '%' ; printing the variable before %
	jne m
mov esi , offset modeq

add esi , 29 ; moving the esi to the next data v want to print 
mov ecx , 5 ; printing , ah

f:
	mov eax , [esi]
	call writechar
	inc esi
	loop f
popad
ret
modequa endp


;-----------------------------------------------------------------------------
;-------------------------- Subtract Equal -----------------------------------
;-----------------------------------------------------------------------------

subequal proc
.data
se byte "mov al , ",0dh , 0ah,"sub al , ",0dh , 0ah,"mov  , al",0
.code
pushad

mov leng , 0
mov esi , offset se ;moving the Offset of the String 
mov ecx , 9 ; want to print till  al ,

p:
    mov eax , [esi]
    call writechar
    inc esi
    loop p

mov esi , offset mystr ;moving offset of the Current Instruction


q:
   mov eax , [esi]
   call writechar
   inc esi
   inc leng  ; incrementing leng for latter use
   mov bl,[esi]
   cmp bl , '-'
   jne q

inc leng ; for skiping '-'

mov esi , offset se
add esi , 9 ; Skiping the previous characters
mov ecx , 11; for printing till sub al ,
r:
    mov eax , [esi]
    call writechar
    inc esi 
    loop r
inc leng ; for skip '='
mov esi , offset mystr
movzx eax , leng ; for printing the charcters after the -=
add esi , eax 
s:
    mov eax , [esi]
    call writechar ; printing the characters from the instructions
    inc esi
    inc leng
    movzx eax ,leng
    cmp al , lengthofinput
    jl s
mov esi ,offset se
add esi , 20 ; Skiping the previous Data
mov ecx , 6 ; printing mov 
t:
  mov eax , [esi]
  call writechar
  inc esi
  loop t
mov esi , offset mystr 

u:
   mov eax , [esi]
   call writechar ; printing data till '-'
   inc esi
   mov bl,[esi]
   cmp bl , '-'
   jne u ; cmparing the character with - and repeating the printing till - comes 
mov esi , offset se
add esi , 26 ; skiping 
mov ecx , 5 ; For the loop to print next 5 characters 
; printing , al
v:
    mov eax , [esi]
    call writechar
    inc esi 
    loop v
popad
ret 
subequal endp



;-----------------------------------------------------------------------------
;------------------------------ Subtraction ----------------------------------
;-----------------------------------------------------------------------------

Subs proc
.data

Subtra byte "mov al ,",0dh , 0ah,"mov bl , ",0dh , 0ah,"Sub al , bl ",0dh , 0ah,"mov , al", 0

.code
pushad
	mov esi , offset subtra
	mov ecx , 8 ;printing  mov al ,
	p:
		mov eax , [esi]
		call writechar
		inc esi
	loop p

	mov leng , 1
	mov esi , offset mystr	
	jj:
	inc leng
	mov eax , [esi]
	inc esi
	mov ebx , [esi]
	cmp bl , '='
	jne jj
	inc esi
	inc leng
q:
		mov eax , [esi]
		call writechar
		inc esi
		inc leng
		mov bl , [esi]
		cmp bl , '-'
		jne q
	mov esi , offset subtra
	add esi , 8
	
	mov ecx , 10
	r:
		mov eax , [esi]
		call writechar
		inc esi
		loop r
	
	mov esi , offset mystr
	movzx eax , leng
	
	add esi , eax
	movzx ecx , lengthofinput
	
	sub ecx ,eax
	s:
		mov eax , [esi]
		call writechar
		inc esi
		loop s
	mov esi , offset subtra
	add esi , 18   ; 8+10
	mov ecx , 21  
	t:
		mov eax , [esi]
		call writechar
		inc esi
		loop t
	
	mov esi , offset mystr	
	u:
	mov eax , [esi]
	call writechar
	inc esi
	mov ebx , [esi]
	cmp bl , '='	
	jne u
	
	mov esi , offset subtra
	add esi , 39
	mov ecx , 4
	v:
	mov eax , [esi]
	call writechar 
	inc esi
	loop v
popad
ret
subs endp

;-----------------------------------------------------------------------------
;-----------------------------MUltilpy Equal----------------------------------
;-----------------------------------------------------------------------------

mulequal proc
.data
meq byte "movzx ax , mov bl ,mul blmov  , al",0 
.code
pushad
mov leng , 0

 mov ecx , 10
 mov esi , offset meq
 
 p:
	mov eax , [esi]
	call writechar
	inc esi

	loop p ; printing till al ,

mov esi , offset mystr

q:
	mov eax , [esi]
	call writechar
	inc esi
	inc leng
	mov ebx , [esi]	
	cmp bl , '*'
	jne q
inc leng
inc leng
call crlf

mov esi , offset meq
add esi , 11 
mov ecx , 8
e:
	mov eax , [esi]
	call writechar
	inc esi
	loop e

mov esi , offset mystr
movzx eax , leng
add esi , eax
mov cl , lengthofinput
sub cl , leng

t:
	mov eax , [esi]
	inc esi
	call writechar
	loop t
call crlf
mov esi , offset meq
add esi , 19  ; 11 + 8
mov eax , [esi]
mov ecx , 6
o:
	mov eax , [esi]
	call writechar
	inc esi
	loop o
call crlf
mov ecx , 4
w:
	mov eax , [esi]
	call writechar
	inc esi
	loop w
mov esi , offset mystr

m:
	mov ax , [esi]
	inc esi
	call writechar
	mov bx , [esi]
	cmp bl , '*'
	jne m
mov esi , offset meq

add esi , 29 ; 11+8+6+4
mov ecx , 5

f:
	mov eax , [esi]
	call writechar
	inc esi
	loop f
popad
ret
mulequal endp

;-----------------------------------------------------------------------------
;---------------------------------- Multiply----------------------------------
;-----------------------------------------------------------------------------

multiply proc
.data
multi byte "mov al ,",0dh , 0ah,"mov bl , ",0dh , 0ah,"mul al , bl ",0dh ,0ah,"mov , al", 0
.code

pushad
	mov esi , offset multi
	mov ecx , 8
	p:
		mov eax , [esi]
		call writechar
		inc esi
	loop p

	mov leng , 1
	mov esi , offset mystr	
	jj:
	inc leng
	mov eax , [esi]
	inc esi
	mov ebx , [esi]
	cmp bl , '='
	jne jj
	inc esi
	inc leng
k:
		mov eax , [esi]
		call writechar
		inc esi
		inc leng
		mov bl , [esi]
		cmp bl , '*'
		jne k
	mov esi , offset multi
	add esi , 8
	
	mov ecx , 10
	j:
		mov eax , [esi]
		call writechar
		inc esi
		loop j
	
	mov esi , offset mystr
	movzx eax , leng
	
	add esi , eax
	movzx ecx , lengthofinput
	
	sub ecx ,eax
	abcx:
		mov eax , [esi]
		call writechar
		inc esi
		loop abcx
	mov esi , offset multi
	add esi , 18   ; 8+10
	mov ecx , 21  
	jkl:
		mov eax , [esi]
		call writechar
		inc esi
		loop jkl
	
	mov esi , offset mystr	
	jk:
	mov eax , [esi]
	call writechar
	inc esi
	mov ebx , [esi]
	cmp bl , '='	
	jne jk
	
	mov esi , offset multi
	add esi , 39
	mov ecx , 4
	
        hj:
	mov eax , [esi]
	call writechar 
	inc esi
	loop hj
popad	
ret
multiply endp

;-----------------------------------------------------------------------------
;-------------------------------Plus------------------------------------------
;-----------------------------------------------------------------------------

addition proc
.data
adding byte "mov al ,",0dh , 0ah,"mov bl , ",0dh , 0ah,"add al , bl ",0dh , 0ah,"mov , al", 0
.code
pushad
	mov esi , offset adding
	mov ecx , 8
	p:
		mov eax , [esi]
		call writechar
		inc esi
	loop p

	mov leng , 1
	mov esi , offset mystr	
	jj:
	inc leng
	mov eax , [esi]
	inc esi
	mov ebx , [esi]
	cmp bl , '='
	jne jj
	inc esi
	inc leng
k:
		mov eax , [esi]
		call writechar
		inc esi
		inc leng
		mov bl , [esi]
		cmp bl , '+'
		jne k
	mov esi , offset adding
	add esi , 8
	
	mov ecx , 10
	j:
		mov eax , [esi]
		call writechar
		inc esi
		loop j
	
	mov esi , offset mystr
	movzx eax , leng
	
	add esi , eax
	movzx ecx , lengthofinput
	
	sub ecx ,eax
	abcx:
		mov eax , [esi]
		call writechar
		inc esi
		loop abcx
	mov esi , offset adding
	add esi , 18   ; 8+10
	mov ecx , 21  
	jkl:
		mov eax , [esi]
		call writechar
		inc esi
		loop jkl
	
	mov esi , offset mystr	
	jk:
	mov eax , [esi]
	call writechar
	inc esi
	mov ebx , [esi]
	cmp bl , '='	
	jne jk
	
	mov esi , offset adding
	add esi , 39
	mov ecx , 4
	hj:
	mov eax , [esi]
	call writechar 
	inc esi
	loop hj	
popad
ret
addition endp

;-----------------------------------------------------------------------------
;------------------------ Plus Equal -----------------------------------------
;-----------------------------------------------------------------------------
plusequal proc
.data
pe byte "mov al , ",0dh , 0ah,"add al , ",0dh , 0ah,"mov  , al",0
.code
pushad
mov leng , 0
mov ecx , 9
mov esi , offset pe
p:
    mov eax , [esi]
    call writechar
    inc esi
    loop p
mov esi , offset mystr 
q:
   mov eax , [esi]
   call writechar
   inc esi
   inc leng
   mov bl,[esi]
   cmp leng , 15
   je r
   cmp bl , '+'
   jne q
inc leng ; for skiping '+'
mov esi , offset pe
add esi , 9
mov ecx , 11
r:
    mov eax , [esi]
    call writechar
    inc esi 
    loop r
inc leng ; for skip '='
mov esi , offset mystr
movzx eax , leng
add esi , eax 
z:
    mov eax , [esi]
    call writechar
    inc esi
    inc leng
    movzx eax ,leng
    cmp al , lengthofinput
    jl z
mov esi ,offset pe
add esi , 20;
mov ecx , 6
x:
  mov eax , [esi]
  call writechar
  inc esi
  loop x
mov esi , offset mystr 
e:
   mov eax , [esi]
   call writechar
   inc esi
   inc leng
   mov bl,[esi]
   cmp leng , 15
   je r
   cmp bl , '+'
   jne e
mov esi , offset pe
add esi , 26
mov ecx , 5

g:
    mov eax , [esi]
    call writechar
    inc esi
    loop g
popad
ret 
plusequal endp

;-------------------------------Bitwise Operations --------------------------

;-----------------------------------------------------------------------------
;----------------------------------Bitwise And--------------------------------
;-----------------------------------------------------------------------------
andop proc
.data
anding byte "mov al ,",0dh , 0ah,"mov bl , ",0dh , 0ah,"And al , bl ",0dh , 0ah,"mov , al", 0
.code
pushad
	mov esi , offset anding
	mov ecx , 8 ;printing mov al ,
	p:
		mov eax , [esi]
		call writechar
		inc esi
	loop p

	mov leng , 1
	mov esi , offset mystr	
q:
	inc leng
	mov eax , [esi]
	inc esi
	mov ebx , [esi]
	cmp bl , '=' ; incrementing till =
	jne q	    
	inc esi  ; skiping =
	inc leng 
r:
		mov eax , [esi]
		call writechar
		inc esi
		inc leng
		mov bl , [esi]
		cmp bl , '&' ;print till $ operator
		jne r
	mov esi , offset anding
	add esi , 8 ; printed till mov al ,
	mov ecx , 10 ; printing enter and mov bl ,
	s:
		mov eax , [esi]
		call writechar
		inc esi
		loop s
	
	mov esi , offset mystr
	movzx eax , leng
	add esi , eax ; mov esi to char next to &
	movzx ecx , lengthofinput
	
	sub ecx ,eax
	t:
		mov eax , [esi]
		call writechar
		inc esi
		loop t

	mov esi , offset anding
	add esi , 18   ; 8+10
	mov ecx , 21  ; printing the array till the 2nd enter before mov ,
	u:
		mov eax , [esi]
		call writechar
		inc esi
		loop u
	
	mov esi , offset mystr	
	v:
	mov eax , [esi]
	call writechar
	inc esi
	mov ebx , [esi]
	cmp bl , '='	; print variable before =
	jne v
	
	mov esi , offset anding
	add esi , 39
	mov ecx , 4 ; printing , al 
	w:
	mov eax , [esi]
	call writechar 
	inc esi
	loop w	
popad
ret
andop endp
;-----------------------------------------------------------------------------
;----------------------------------Bitwise OR---------------------------------
;-----------------------------------------------------------------------------
orop proc
.data
ORR byte "mov al ,",0dh , 0ah,"mov bl , ",0dh , 0ah,"OR  al , bl ",0dh , 0ah,"mov , al", 0
.code
pushad
	
	
	mov esi , offset ORR
	mov ecx , 8
	p:
		mov eax , [esi]
		call writechar
		inc esi
	loop p
	mov leng , 1
	mov esi , offset mystr	
	jj:
	inc leng
	mov eax , [esi]
	inc esi
	mov ebx , [esi]
	cmp bl , '='
	jne jj
	inc esi
	inc leng
k:
		mov eax , [esi]
		call writechar
		inc esi
		inc leng
		mov bl , [esi]
		cmp bl , '|'
		jne k
	mov esi , offset ORR
	add esi , 8
	
	mov ecx , 10
	j:
		mov eax , [esi]
		call writechar
		inc esi
		loop j
	
	mov esi , offset mystr
	movzx eax , leng
	
	add esi , eax
	movzx ecx , lengthofinput
	
	sub ecx ,eax
	abcx:
		mov eax , [esi]
		call writechar
		inc esi
		loop abcx
	mov esi , offset ORR
	add esi , 18   ; 8+10
	mov ecx , 21  
	jkl:
		mov eax , [esi]
		call writechar
		inc esi
		loop jkl
	
	mov esi , offset mystr	
	jk:
	mov eax , [esi]
	call writechar
	inc esi
	mov ebx , [esi]
	cmp bl , '='	
	jne jk
	
	mov esi , offset ORR
	add esi , 39
	mov ecx , 4
	hj:
	mov eax , [esi]
	call writechar 
	inc esi
	loop hj	
popad
ret
orop endp

;------------------------------- Comparision ---------------------------------

;-----------------------------------------------------------------------------
;---------------------------------- IF ---------------------------------------
;-----------------------------------------------------------------------------

ifins proc
pushad
mov eax , '.'
call writechar 
mov esi , offset mystr
q:	
	mov eax , [esi]
	call writechar
	inc esi
	mov ebx , [esi]
	cmp bl , ')' ;coping if printing till ')'
	jne q
 mov eax , ')'	
 call writechar ; printing ')'
 call crlf
popad
ret
ifins endp

;-----------------------------------------------------------------------------
;------------------------------EndIf------------------------------------------
;-----------------------------------------------------------------------------

endins proc
.data
 edf byte ".endif",0 
.code

pushad
mov edx , offset edf ; just printing the String
call writestring

popad
ret
endins endp

;------------------------------- I/O Instructions ----------------------------

;------------------------------ Input Instructions ---------------------------

;-----------------------------------------------------------------------------
;--------------------------------- Input Integer -----------------------------
;-----------------------------------------------------------------------------
inputint proc
.data
inint byte 'call readint mov  ,al'
.code
pushad
mov edx , 0
mov ecx , 13 ; printing call readint
mov esi , offset inint
k:
	mov eax , [esi]
	call writechar
	inc esi
	loop k
call crlf
mov ecx , 4 ; to print mov 
l:
	mov eax , [esi]
	call writechar
	inc esi
	loop l

mov esi , offset mystr
m:
	inc esi
	inc edx
	mov eax , [esi]
	cmp al , '>' ; to skip cin 
	jne m
add esi , 2 ; to skip greater than sign
add edx , 2
mov cl ,lengthofinput
sub ecx , edx ; to print the length of variable after cin>> 
t:
	mov eax , [esi]
	inc esi
	call writechar
	loop t
mov esi , offset inint 
add esi , 18
mov ecx , 3 ;to print , al
u:
	mov eax , [esi]
	inc esi
	call writechar
	loop u
call crlf
popad	
ret
inputint endp


;-----------------------------------------------------------------------------
;------------------------------INPUT CHAR-------------------------------------
;-----------------------------------------------------------------------------
inputchar proc
.data
inchar byte 'call readchar mov  ,al'
.code
pushad
mov edx , 0
mov ecx , 14
mov esi , offset inchar
k:
	mov eax , [esi]
	call writechar
	inc esi
	loop k
call crlf
mov ecx , 4 
l:
	mov eax , [esi]
	call writechar
	inc esi
	loop l

mov esi , offset mystr
m:
	inc esi
	inc edx
	mov eax , [esi]
	cmp al , '>' ; to skip cin
	jne m
add esi , 2 ; to skip greater than sign
add edx , 2
mov cl ,lengthofinput
sub ecx , edx
t:
	mov eax , [esi]
	inc esi
	call writechar
	loop t
mov esi , offset inchar
add esi , 19
mov ecx , 3
u:
	mov eax , [esi]
	inc esi
	call writechar
	loop u
call crlf
popad	
ret
inputchar endp

;--------------------------- Output Instructions -----------------------------

;-----------------------------------------------------------------------------
;------------------------------Output Char------------------------------------
;-----------------------------------------------------------------------------
outputchar proc
.data
outchar byte 'mov al , call writechar'
.code
pushad
mov edx , 0
mov ecx , 8
mov esi , offset outchar
k:
	mov eax , [esi]
	call writechar
	inc esi
	loop k
mov esi , offset mystr
m:
	inc esi
	inc edx
	mov eax , [esi]
	cmp al , '<' ; to skip cout
	jne m
add esi , 2 ; to skip less than sign
add edx , 2
mov cl ,lengthofinput
sub ecx , edx
t:
	mov eax , [esi]
	inc esi
	call writechar
	loop t
call crlf
mov esi , offset outchar
add esi , 9
mov ecx , 14
u:
	mov eax , [esi]
	inc esi
	call writechar
	loop u
call crlf	
popad	
ret
outputchar endp

;-----------------------------------------------------------------------------
;---------------------------------- Output Int -------------------------------
;-----------------------------------------------------------------------------
outputint proc
.data
outint byte 'mov al , call writeint'
.code
pushad
mov edx , 0
mov ecx , 8
mov esi , offset outint
k:
	mov eax , [esi]
	call writechar
	inc esi
	loop k
mov esi , offset mystr
m:
	inc esi
	inc edx
	mov eax , [esi]
	cmp al , '<' ; to skip cin
	jne m
add esi , 2 ; to skip less than sign
add edx , 2
mov cl ,lengthofinput
sub ecx , edx
t:
	mov eax , [esi]
	inc esi
	call writechar
	loop t
call crlf
mov esi , offset outint
add esi , 9
mov ecx , 13
u:
	mov eax , [esi]
	inc esi
	call writechar
	loop u
call crlf	
popad	
ret
outputint endp


;-----------------------------------------------------------------------------
;-----------------------------Output String-----------------------------------
;-----------------------------------------------------------------------------
outputstring proc
.data
outstr byte 'mov edx ,offset  call WriteString'
.code
pushad

mov edx , 0
mov ecx , 17
mov esi , offset outstr
k:
	mov eax , [esi]
	call writechar
	inc esi
	loop k
mov esi , offset mystr
m:
	inc esi
	inc edx
	mov eax , [esi]
	cmp al , '<' ; to skip cin
	jne m
add esi , 2 ; to skip less than sign
add edx , 2
mov cl ,lengthofinput
sub ecx , edx
t:
	mov eax , [esi]
	inc esi
	call writechar
	loop t
call crlf
mov esi , offset outstr
add esi , 17
mov ecx , 19
u:
	mov eax , [esi]
	inc esi
	call writechar
	loop u
call crlf	
popad	
ret
outputstring endp

;-----------------------------------------------------------------------------
;------------------------------Procedure to leave lines-----------------------
;-----------------------------------------------------------------------------
enterproc proc
.data
	en byte 0dh , 0ah, 0
.code
pushad
call Crlf
mov edx ,offset en
call writestring
popad
ret
enterproc endp
;-----------------------------------------------------------------------------
;-----------------------------Main Procedure----------------------------------
;-----------------------------------------------------------------------------
main PROC
;----------------------------------opening File-------------------------------
Invoke createfile ,addr filename , generic_read,do_not_Share,NULL,Open_existing , file_attribute_normal , 0
	
mov filehandle , eax 
.if eax == INVALID_HANDLE_VALUE
mov edx , offset errmsg
call writestring
jmp quitnow
.ENDIF
;----------------Reading instruction form File and storing it in buffer-------
invoke readfile , filehandle , addr buffer , buffersize , addr bytecount , 0
;------------------Closing File handle----------------------------------------
invoke closehandle, filehandle



mov esi , offset buffer; , eax
add esi , bytecount
deletebraket:
	dec esi
	dec bytecount
	mov eax , [esi]
	cmp al , '}'
	jne deletebraket
MOV EAX , ' '
MOV [ESI],EAX

mov ecx, 0
mov esi , offset buffer
mov eax , ' '
q:
inc ecx 
inc esi
mov eax , [esi]
cmp al , '{'
jne q
add esi , 2 ; to skip enter and '{'
ADD ecx , 2 ; to skip '{'
mov al , ' ' ; removing '{'
readinstructions:

    mov ebx , offset mystr
    mov lengthofinput ,0

.while (  al != ';' && al !=')' && al!='}')
  
  inc ecx
  cmp ecx , bytecount
  jg quitnow ; checking buffer and comparing the memory
  inc lengthofinput
  mov [ebx] , eax ; moving string from buffer to mystr
  inc esi         ; incrementing pointer of buffer
  inc ebx 	  ; incremnting pointer of mystr
  mov eax , [esi]
;---------------Checking if the current character is operator or not--------
 .if (al == '+' || al == '-' || al == '='  || al == '*' || al == '/' || al == '&' || al == '|' || al == '%' || al == '(' || al =='}' || al=='>' ||al=='<')
		push ecx
		push ebx
		mov cl , op2
		mov bl , op1
		cmp op1 , '('
		je ou
		CMP BL ,'0'
		JE B 
		cmp cl, '0' 
		JE K
		

		jmp operr ; there are 3 operators in single statement
		
		K:
		mov op2,al
		JMP OU
		
		B:
		
		mov op1,al
		;call writechar
		ou:	
		pop ebx
		pop ecx
	.endif 
;-----------------------------------------------------------------------------
;------------------------------------END OF instruction-----------------------	
;-----------------------------------------------------------------------------
.endw
endwhile:  
  mov [ebx] , eax   ;Moving last character from  buffer to instruction String
  inc ebx
  mov eax , 0
  inc ecx
  mov [ebx] , eax
  add esi , 2
;---------------------------Checking For = operator---------------------------
  .IF (OP1 == '=')
	
	cmp op2 , '0'
	je equa
	cmp op2 , '+'
	je ea	
	cmp op2 , '-'
	je esub
		cmp op2 , '*'
	je em
 	cmp op2 , '/'
	je ed
	cmp op2 , '&'
	je eande
 	cmp op2 , '|'
	je eo
	cmp op2 , '%'
        je emodul
;-------------call Diffrent Procedures according to Instruction---------------
	ed:
		call did
		jmp e3
	em:
		call multiply
		jmp e3
	esub:
		call Subs
		jmp e3
	
	ea:
		CALL addition
		jmp e3
	eande:
		call andop
		jmp e3
	eo:
		call orop
		jmp e3
	emodul: 
		call remain
		jmp e3
	equa:
	call assin
	jmp e3
   e3:	; OPERATOR CHECKING ENDS
  .ENDIF

  .if (op1 =='+')
	cmp op2 ,'+'
	je incre
  	cmp op2 , '='
   	je adeq
	jmp operr
	incre:
		call increment
		jmp ade
	adeq:
		 call plusequal
		jmp ade
	ade: ; ADD END
  .endif
  .if (op1 =='-')
	cmp op2 ,'-'
	je decr
  	cmp op2 , '='
   	je subeq
	jmp operr
	
	decr:
		call decre
		jmp sube
	subeq:
		call subequal
		jmp sube
	sube: ; subtract END
  .endif
  .if (op1 =='*')
	cmp op2 , '=' 
	jne operr ; if operator not '='thre is an error 
  	call mulequal; call function
  .endif

  .if (op1 =='/')
	cmp op2 , '='
       jne operr
  	  call dive
   
   endofdivide:	
  .endif
  

  .if (op1 =='%')
	cmp op2 , '='
	jne operr ; if operator not '='thre is an error
  	call modequa
  .endif

  .if (op1 =='}')
	call endins
  .endif

  .if(op1 == '(')
	call ifins
	inc ecx
	inc esi  
  .endif


.if( op1 == '>' && op2 =='>')
pushad
	mov esi , offset mystr
	m:
	inc esi
	inc edx
	mov eax , [esi]
	cmp al , '>' ; to skip cin
	jne m
	add esi , 2 ; to skip greater than sign
	add edx , 2
	mov eax , [esi]
	cmp al , 97 
;Our parser's Restiction we are Asuming that small alphabet characters
;are integer integers and Capital leters are char
;
	jl charin
	call inputint
	jmp endinput
	charin:
	call inputchar

endinput:
popad
.endif


.if( op1 == '<' && op2 =='<')
pushad
        mov esi , offset mystr
	skipin:
	inc esi
	;inc edx
	mov eax , [esi]
	cmp al , '<' ; to skip cout
	jne skipin
	add esi , 2 ; to skip greater than sign
	;add edx , 2
	mov eax , [esi]
	cmp al , 's'
	 je string
	cmp al , 'S'
;Our parser's Restiction we are Asuming that small alphabet characters
;are integer integers and Capital leters are char
;and in output variables strating with s are strings so we are treating thm as
;string
 	 je string
	cmp al , 39 ; cmparing '
	je ouchar
	cmp al , '9'
	jle intout
	cmp al , 97
	jl ouchar
	intout:
	call outputint
	jmp endout
	
	string:
		call outputstring
	jmp endout
	ouchar:
	call outputchar

endout:


popad
.endif


mov op1 , '0'
mov op2 , '0'

call enterproc
call clr
;----------------------- Comparing Ecx With the Size Of Input-----------------
cmp ecx , bytecount
jl readinstructions

;-----------------------------------------------------------------------------
;------------------------Jump To EXIT-----------------------------------------
;-----------------------------------------------------------------------------

jmp quitnow

;-----------------------------------------------------------------------------
;------------------------------Error -----------------------------------------
;-----------------------------------------------------------------------------
operr:  ; error mssage

mov edx , offset er
call writestring
quitnow:
call readchar ; for halting the outputscreen
	exit
main ENDP
END main