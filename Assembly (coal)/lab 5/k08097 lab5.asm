TITLE Lab5

; 
; 
; 

INCLUDE Irvine32.inc

.data
	mesg BYTE 'Procedures are an important part of the Structured Programming Paradigm'

.code
PrintString PROC

	DoLoop:	
		; transfer the character, increment index and print char
		mov al, [esi]
		inc esi
		call WriteChar
	
		; decrement ecx, compare it to zero and then invoke the label if not equal
		dec ecx
		cmp ecx, 0
		jne DoLoop	

	ret
PrintString ENDP

UpperCase PROC

	Work:
		; decrement ecx, the number of times loop is to be repeated
		dec ecx
		
		; skip letters other than uppercase letters
		mov eax, 0
		mov al, [esi]
		cmp eax, 97
		jb IncESI
		cmp eax, 122
		ja IncESI
	
		; subtract 32 from the character at [esi]
		mov al, [esi]
		sub al, 32
		mov [esi], al
		inc esi
		
		; if ecx not equals to zero, loop again
		cmp ecx, 0
		jne Work

	; printing string
	mov esi, offset mesg
	mov ecx, lengthof mesg 
	call PrintString
	
	ret	
	
	IncESI:
		inc esi
		jmp Work

UpperCase ENDP

Mult PROC

	mov ecx, eax
	jmp Check

	Adding:
		add ecx, eax	
	
	Check:
		dec ebx
		cmp ebx, 0
		jne Adding

	mov eax, ecx
	
	ret
Mult ENDP

main PROC

	; Task 2
	mov esi, offset mesg
	mov ecx, lengthof mesg		
	call PrintString

	mov al, 10
	call WriteChar

	; Task 3
	mov esi, offset mesg
	mov ecx, lengthof mesg
	call UpperCase

	mov al, 10
	call WriteChar

	; Task 4
	call ReadInt
	mov ebx, eax
	call ReadInt
	call Mult
	call WriteInt

	exit
main ENDP
END main