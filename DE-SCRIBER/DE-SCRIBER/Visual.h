#pragma once
#include "VDLL.h"
#include "Cursor.h"
//#include "RobinKarp.h"

using namespace System;
using namespace System::Windows::Forms;

namespace DESCRIBER{

class Visual
{

#pragma region Class Members
private:
	VisualDoublyLinkedList<char*>* _MyLine;//700x585
	unsigned int _CurLine;
	unsigned int MAX_Lines;
public:
	Cursor _MyWord;
	Cursor _MyBackUpWord;
	Cursor _CopyWord;
	bool isChanged;
	bool reLoad;

#pragma endregion


public:

	Visual(void)
	{
		MAX_Lines = 1000;
		_CurLine = 0;
		isChanged = false;
		reLoad = false;
		_MyLine = new VisualDoublyLinkedList<char*> [MAX_Lines];
	}
	void ClearWord()
	{
		Cursor Null;
		_MyWord = Null;
	}
	void StartUp(Form^ MyForm)
	{
		if (MyForm->Controls->ContainsKey("WordPanel"))
			MyForm->Controls["WordPanel"]->Controls->Clear();
	}
	unsigned int CurLine()
	{
		return _CurLine;
	}
	void CurLine(unsigned int p)
	{
		_CurLine = p;
	}
	void getLabelID(unsigned int i, unsigned j, unsigned int k, char id[])
	{
		if(i <10)
		{
			*(id+0)='0';
		}
		else
		{
			*(id+0)=Convert::ToChar(((i/100)%10)+48);
		}
		*(id+1)=Convert::ToChar((i%10)+48);
		
		if(j <10)
		{
			*(id+2)='0';
		}
		else
		{
			*(id+2)=Convert::ToChar(((j/100)%10)+48);
		}
		*(id+3)=Convert::ToChar((j%10)+48);
		if(k <10)
		{
			*(id+4)='0';
		}
		else
		{
			*(id+4)=Convert::ToChar(((k/100)%10)+48);
		}
		*(id+5)=Convert::ToChar((k%10)+48);
	}

	
	void getLabelID(char* id , unsigned int lblid[])
	{
		lblid[0] = (int) *(id+0);
		lblid[0] *=10;
		lblid[0] = (int) *(id+1);
		lblid[1] = (int) *(id+2);
		lblid[1] *=10;
		lblid[1] = (int) *(id+3);
		lblid[2] = (int) *(id+4);
		lblid[2] *=10;
		lblid[2] = (int) *(id+5);
	}

	void AddWord(unsigned int i, unsigned int j, unsigned int k,char* ch)
	{
		char* id = new char[6];
		getLabelID(i,j,k,id);
		_MyLine[0].InsertAtLast(ch,id,0,0,0,0);
	}

	void FindNReplace()
	{
		VDNode<char*>* Temp;
		Temp = _MyLine[0].getHead();
		while (Temp!=NULL)
		{
			bool Found = false;
			char* MyWord = new char;
			MyWord = Temp->Data();
			//RobinKarp::fnr(Temp->Data(),"fast","fast",&tee);
			if(Found)
				MessageBox::Show("Found");
			Temp = Temp->Next();
		}
	}

	bool CompleteCursor(Form^MyForm, char key, unsigned int retID[], char* ret)
	{
		if (!_MyWord.isEmpty())
		{
		_MyWord.AssignWord(ret);
		unsigned int p[3] = {0,0,0};
		getLabelID(_MyWord.LblID(),p);
		for (Byte i=0; i<3; i++)
			retID[i] = p[i];
		_MyLine[_CurLine].Insert(p[3],_MyWord.Word(),_MyWord.LblID(),0,0,0,0);
		Cursor Null;
		_MyWord = Null;
		_MyBackUpWord = Null;
		if ( (key == 13) || (key == 190) )//enter--full stop
			_CurLine++;
		return true;
		}
		return false;
	}

	void AddChar(Form^ MyForm, char ch)
	{
		this->reLoad = true;
		_MyBackUpWord = _MyWord;
		_MyWord.AddChar(MyForm, ch);
		//Add visual char
		int p[3] = {0,0,0};
		//char* TempWord = new char;
		//TempWord = _MyWord.LabelID(p); Not used label ID
		if (MyForm->Controls->ContainsKey("WordPanel"))
		{
			if (!MyForm->Controls["WordPanel"]->Controls->ContainsKey("CLabel"))
			{
				Label^ CLabel = gcnew Label();
				CLabel->Name = "CLabel";
				CLabel->Location = Drawing::Point(10,10);
				CLabel->Size = Drawing::Size(Drawing::Point(600,500));
				CLabel->AutoSize = true;
				CLabel->BackColor = System::Drawing::Color::Transparent;
				MyForm->Controls["WordPanel"]->Controls->Add(CLabel);
			}
			MyForm->Controls["WordPanel"]->Controls["CLabel"]->Text += Convert::ToChar(ch);
		}
	}
};

};
