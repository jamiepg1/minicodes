#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
#include "Controller.h"
#include "SpellCheck.h"


namespace DESCRIBER {

	/// <summary>
	/// Summary for MAINFORM
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class MAINFORM : public System::Windows::Forms::Form
	{

#pragma region Class Functions

	public:
		MAINFORM(void)
		{
			InitializeComponent();
			this->_Manager = new Controller();
			//
			//TODO: Add the constructor code here
			//
		}

		MAINFORM(Controller* obj)
		{
			this->_Manager = obj;
			this->InitializeComponent();
			this->_Manager->StartUp(this);
			//
			//TODO: Add the constructor code here
			//
			//this->InitForm();
			//this->Control_Bar = 0;
			/*ProcessManager->RunWorkerAsync();
			ProcessManager->InitializeLifetimeService();*/
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MAINFORM()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::ComponentModel::IContainer^  components;
	protected: 

#pragma endregion

#pragma region Class Members
		/// <summary>
		/// Required designer variable.
		/// </summary>

	private: System::Windows::Forms::Panel^  Top_Controls;
	private: System::Windows::Forms::Panel^  Left_Pad;
	private: System::Windows::Forms::Panel^  Right_Tab;
	private: System::Windows::Forms::Panel^  Bottom_Pad;

	private: System::Windows::Forms::PictureBox^  Button_Menu;
	private: System::Windows::Forms::PictureBox^  Button_Minimize;
	private: System::Windows::Forms::PictureBox^  Button_Close;
	private: System::Windows::Forms::PictureBox^  Image_Scriber;
	private: System::Windows::Forms::Panel^  Controls_panel1;
	private: System::Windows::Forms::Button^  Button_Control1;
	private: System::Windows::Forms::Panel^  Menu_Main;
	private: System::Windows::Forms::Button^  Button_MenuOpen;
	private: System::Windows::Forms::Button^  Button_Control3;
	private: System::Windows::Forms::Button^  Button_Control2;
	private: System::Windows::Forms::Button^  Button_MenuNew;
	private: System::Windows::Forms::Button^  Button_MenuSaveAs;
	private: System::Windows::Forms::Button^  Button_MenuSave;
	private: System::Windows::Forms::Button^  Button_MenuClose;
	private: System::Windows::Forms::Label^  Label_MenuDescription;



	private: DESCRIBER::Controller* _Manager;
	private: System::ComponentModel::BackgroundWorker^  ProcessManager;


	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Button^  button2;




	private: System::Windows::Forms::Button^  button8;
	private: System::Windows::Forms::Button^  button7;
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::Button^  button11;
	private: System::Windows::Forms::Button^  button10;
	private: System::Windows::Forms::Button^  button12;
	private: System::Windows::Forms::RichTextBox^  Main_Panel;
	private: System::Windows::Forms::Button^  button9;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog1;
	private: System::Windows::Forms::TabControl^  Tab_Panel;

	private: System::Windows::Forms::TabPage^  tabPage1;
	private: System::Windows::Forms::Panel^  Replace_Panel;

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
private: System::Windows::Forms::TextBox^  Replace_Text;

private: System::Windows::Forms::TextBox^  Find_Text;
private: System::Windows::Forms::Panel^  Font_Panel;

private: System::Windows::Forms::Label^  label5;
private: System::Windows::Forms::Button^  ColPanel_Red;



private: System::Windows::Forms::Label^  label6;
private: System::Windows::Forms::Button^  ColPanel_Blue;
private: System::Windows::Forms::Button^  ColPanel_Black;

private: System::Windows::Forms::Button^  ColPanel_Yellow;
private: System::Windows::Forms::Button^  ColPanel_Green;
private: System::Windows::Forms::Timer^  timer1;












	private: int Control_Bar;

 #pragma endregion

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MAINFORM::typeid));
			this->Top_Controls = (gcnew System::Windows::Forms::Panel());
			this->Button_Control3 = (gcnew System::Windows::Forms::Button());
			this->Button_Control2 = (gcnew System::Windows::Forms::Button());
			this->Button_Control1 = (gcnew System::Windows::Forms::Button());
			this->Controls_panel1 = (gcnew System::Windows::Forms::Panel());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->button12 = (gcnew System::Windows::Forms::Button());
			this->button11 = (gcnew System::Windows::Forms::Button());
			this->button10 = (gcnew System::Windows::Forms::Button());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->Image_Scriber = (gcnew System::Windows::Forms::PictureBox());
			this->Button_Minimize = (gcnew System::Windows::Forms::PictureBox());
			this->Button_Close = (gcnew System::Windows::Forms::PictureBox());
			this->Button_Menu = (gcnew System::Windows::Forms::PictureBox());
			this->Left_Pad = (gcnew System::Windows::Forms::Panel());
			this->Right_Tab = (gcnew System::Windows::Forms::Panel());
			this->Font_Panel = (gcnew System::Windows::Forms::Panel());
			this->ColPanel_Black = (gcnew System::Windows::Forms::Button());
			this->ColPanel_Yellow = (gcnew System::Windows::Forms::Button());
			this->ColPanel_Green = (gcnew System::Windows::Forms::Button());
			this->ColPanel_Blue = (gcnew System::Windows::Forms::Button());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->ColPanel_Red = (gcnew System::Windows::Forms::Button());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->Replace_Panel = (gcnew System::Windows::Forms::Panel());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->Replace_Text = (gcnew System::Windows::Forms::TextBox());
			this->Find_Text = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->Bottom_Pad = (gcnew System::Windows::Forms::Panel());
			this->Menu_Main = (gcnew System::Windows::Forms::Panel());
			this->Label_MenuDescription = (gcnew System::Windows::Forms::Label());
			this->Button_MenuClose = (gcnew System::Windows::Forms::Button());
			this->Button_MenuSaveAs = (gcnew System::Windows::Forms::Button());
			this->Button_MenuSave = (gcnew System::Windows::Forms::Button());
			this->Button_MenuNew = (gcnew System::Windows::Forms::Button());
			this->Button_MenuOpen = (gcnew System::Windows::Forms::Button());
			this->ProcessManager = (gcnew System::ComponentModel::BackgroundWorker());
			this->Main_Panel = (gcnew System::Windows::Forms::RichTextBox());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->Tab_Panel = (gcnew System::Windows::Forms::TabControl());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->Top_Controls->SuspendLayout();
			this->Controls_panel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->Image_Scriber))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->Button_Minimize))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->Button_Close))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->Button_Menu))->BeginInit();
			this->Right_Tab->SuspendLayout();
			this->Font_Panel->SuspendLayout();
			this->Replace_Panel->SuspendLayout();
			this->Menu_Main->SuspendLayout();
			this->Tab_Panel->SuspendLayout();
			this->tabPage1->SuspendLayout();
			this->SuspendLayout();
			// 
			// Top_Controls
			// 
			this->Top_Controls->BackColor = System::Drawing::Color::Transparent;
			this->Top_Controls->Controls->Add(this->Button_Control3);
			this->Top_Controls->Controls->Add(this->Button_Control2);
			this->Top_Controls->Controls->Add(this->Button_Control1);
			this->Top_Controls->Controls->Add(this->Controls_panel1);
			this->Top_Controls->Controls->Add(this->Image_Scriber);
			this->Top_Controls->Controls->Add(this->Button_Minimize);
			this->Top_Controls->Controls->Add(this->Button_Close);
			this->Top_Controls->Controls->Add(this->Button_Menu);
			this->Top_Controls->Dock = System::Windows::Forms::DockStyle::Top;
			this->Top_Controls->Location = System::Drawing::Point(0, 0);
			this->Top_Controls->Name = L"Top_Controls";
			this->Top_Controls->Size = System::Drawing::Size(1100, 150);
			this->Top_Controls->TabIndex = 0;
			// 
			// Button_Control3
			// 
			this->Button_Control3->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->Button_Control3->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 14.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->Button_Control3->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Button_Control3.Image")));
			this->Button_Control3->Location = System::Drawing::Point(599, 14);
			this->Button_Control3->Margin = System::Windows::Forms::Padding(0);
			this->Button_Control3->Name = L"Button_Control3";
			this->Button_Control3->Size = System::Drawing::Size(100, 32);
			this->Button_Control3->TabIndex = 7;
			this->Button_Control3->TabStop = false;
			this->Button_Control3->Text = L"SETTING";
			this->Button_Control3->UseVisualStyleBackColor = true;
			// 
			// Button_Control2
			// 
			this->Button_Control2->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->Button_Control2->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 14.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->Button_Control2->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Button_Control2.Image")));
			this->Button_Control2->Location = System::Drawing::Point(499, 14);
			this->Button_Control2->Margin = System::Windows::Forms::Padding(0);
			this->Button_Control2->Name = L"Button_Control2";
			this->Button_Control2->Size = System::Drawing::Size(100, 32);
			this->Button_Control2->TabIndex = 6;
			this->Button_Control2->TabStop = false;
			this->Button_Control2->Text = L"VOICE";
			this->Button_Control2->UseVisualStyleBackColor = true;
			// 
			// Button_Control1
			// 
			this->Button_Control1->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->Button_Control1->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 14.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->Button_Control1->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Button_Control1.Image")));
			this->Button_Control1->Location = System::Drawing::Point(399, 14);
			this->Button_Control1->Margin = System::Windows::Forms::Padding(0);
			this->Button_Control1->Name = L"Button_Control1";
			this->Button_Control1->Size = System::Drawing::Size(100, 32);
			this->Button_Control1->TabIndex = 5;
			this->Button_Control1->TabStop = false;
			this->Button_Control1->Text = L"TEXT";
			this->Button_Control1->UseVisualStyleBackColor = true;
			this->Button_Control1->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MAINFORM::MAINFORM_KeyDown);
			// 
			// Controls_panel1
			// 
			this->Controls_panel1->BackgroundImage = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Controls_panel1.BackgroundImage")));
			this->Controls_panel1->Controls->Add(this->button9);
			this->Controls_panel1->Controls->Add(this->button12);
			this->Controls_panel1->Controls->Add(this->button11);
			this->Controls_panel1->Controls->Add(this->button10);
			this->Controls_panel1->Controls->Add(this->button8);
			this->Controls_panel1->Controls->Add(this->button7);
			this->Controls_panel1->Controls->Add(this->button6);
			this->Controls_panel1->Controls->Add(this->button2);
			this->Controls_panel1->Controls->Add(this->button5);
			this->Controls_panel1->Controls->Add(this->button4);
			this->Controls_panel1->Controls->Add(this->button3);
			this->Controls_panel1->Location = System::Drawing::Point(10, 44);
			this->Controls_panel1->Name = L"Controls_panel1";
			this->Controls_panel1->Size = System::Drawing::Size(1084, 103);
			this->Controls_panel1->TabIndex = 4;
			// 
			// button9
			// 
			this->button9->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->button9->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button9->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button9->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"button9.Image")));
			this->button9->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button9->Location = System::Drawing::Point(180, 37);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(65, 20);
			this->button9->TabIndex = 14;
			this->button9->TabStop = false;
			this->button9->Text = L"REDO";
			this->button9->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button9->UseVisualStyleBackColor = true;
			this->button9->Click += gcnew System::EventHandler(this, &MAINFORM::button9_Click);
			// 
			// button12
			// 
			this->button12->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->button12->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button12->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button12->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"button12.Image")));
			this->button12->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button12->Location = System::Drawing::Point(325, 64);
			this->button12->Name = L"button12";
			this->button12->Size = System::Drawing::Size(65, 20);
			this->button12->TabIndex = 13;
			this->button12->TabStop = false;
			this->button12->Text = L"XCHG";
			this->button12->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button12->UseVisualStyleBackColor = true;
			this->button12->Click += gcnew System::EventHandler(this, &MAINFORM::button12_Click);
			// 
			// button11
			// 
			this->button11->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->button11->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button11->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 9.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button11->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"button11.Image")));
			this->button11->ImageAlign = System::Drawing::ContentAlignment::BottomLeft;
			this->button11->Location = System::Drawing::Point(470, 37);
			this->button11->Name = L"button11";
			this->button11->Size = System::Drawing::Size(65, 50);
			this->button11->TabIndex = 12;
			this->button11->TabStop = false;
			this->button11->Text = L"FIND";
			this->button11->TextAlign = System::Drawing::ContentAlignment::TopRight;
			this->button11->UseVisualStyleBackColor = true;
			this->button11->Click += gcnew System::EventHandler(this, &MAINFORM::button11_Click);
			// 
			// button10
			// 
			this->button10->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->button10->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button10->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button10->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"button10.Image")));
			this->button10->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button10->Location = System::Drawing::Point(470, 10);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(65, 20);
			this->button10->TabIndex = 11;
			this->button10->TabStop = false;
			this->button10->Text = L"GOTO";
			this->button10->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button10->UseVisualStyleBackColor = true;
			this->button10->Click += gcnew System::EventHandler(this, &MAINFORM::button10_Click);
			// 
			// button8
			// 
			this->button8->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->button8->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button8->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button8->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"button8.Image")));
			this->button8->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button8->Location = System::Drawing::Point(325, 37);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(65, 20);
			this->button8->TabIndex = 9;
			this->button8->TabStop = false;
			this->button8->Text = L"FONT";
			this->button8->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button8->UseVisualStyleBackColor = true;
			this->button8->Click += gcnew System::EventHandler(this, &MAINFORM::button8_Click);
			// 
			// button7
			// 
			this->button7->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->button7->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button7->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 9.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button7->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"button7.Image")));
			this->button7->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button7->Location = System::Drawing::Point(325, 10);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(65, 20);
			this->button7->TabIndex = 8;
			this->button7->TabStop = false;
			this->button7->Text = L"FONT";
			this->button7->TextAlign = System::Drawing::ContentAlignment::TopRight;
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += gcnew System::EventHandler(this, &MAINFORM::button7_Click);
			// 
			// button6
			// 
			this->button6->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->button6->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button6->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button6->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"button6.Image")));
			this->button6->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button6->Location = System::Drawing::Point(180, 64);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(65, 20);
			this->button6->TabIndex = 7;
			this->button6->TabStop = false;
			this->button6->Text = L"UNDO";
			this->button6->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &MAINFORM::button6_Click);
			// 
			// button2
			// 
			this->button2->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->button2->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button2->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button2->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"button2.Image")));
			this->button2->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button2->Location = System::Drawing::Point(180, 10);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(65, 20);
			this->button2->TabIndex = 6;
			this->button2->TabStop = false;
			this->button2->Text = L"COLOR";
			this->button2->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MAINFORM::button2_Click);
			// 
			// button5
			// 
			this->button5->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->button5->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button5->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button5->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"button5.Image")));
			this->button5->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button5->Location = System::Drawing::Point(30, 64);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(65, 20);
			this->button5->TabIndex = 4;
			this->button5->TabStop = false;
			this->button5->Text = L"PASTE";
			this->button5->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &MAINFORM::button5_Click);
			// 
			// button4
			// 
			this->button4->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->button4->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button4->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button4->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"button4.Image")));
			this->button4->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button4->Location = System::Drawing::Point(30, 37);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(65, 20);
			this->button4->TabIndex = 3;
			this->button4->TabStop = false;
			this->button4->Text = L"COPY";
			this->button4->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &MAINFORM::button4_Click);
			// 
			// button3
			// 
			this->button3->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->button3->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->button3->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->button3->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"button3.Image")));
			this->button3->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->button3->Location = System::Drawing::Point(30, 10);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(65, 20);
			this->button3->TabIndex = 2;
			this->button3->TabStop = false;
			this->button3->Text = L"CUT";
			this->button3->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MAINFORM::button3_Click);
			// 
			// Image_Scriber
			// 
			this->Image_Scriber->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Image_Scriber.Image")));
			this->Image_Scriber->Location = System::Drawing::Point(48, 0);
			this->Image_Scriber->Name = L"Image_Scriber";
			this->Image_Scriber->Size = System::Drawing::Size(175, 40);
			this->Image_Scriber->TabIndex = 3;
			this->Image_Scriber->TabStop = false;
			// 
			// Button_Minimize
			// 
			this->Button_Minimize->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Button_Minimize.Image")));
			this->Button_Minimize->Location = System::Drawing::Point(1020, 0);
			this->Button_Minimize->Name = L"Button_Minimize";
			this->Button_Minimize->Size = System::Drawing::Size(35, 40);
			this->Button_Minimize->TabIndex = 2;
			this->Button_Minimize->TabStop = false;
			this->Button_Minimize->Click += gcnew System::EventHandler(this, &MAINFORM::Button_Minimize_Click);
			// 
			// Button_Close
			// 
			this->Button_Close->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Button_Close.Image")));
			this->Button_Close->Location = System::Drawing::Point(1064, 0);
			this->Button_Close->Name = L"Button_Close";
			this->Button_Close->Size = System::Drawing::Size(35, 40);
			this->Button_Close->TabIndex = 1;
			this->Button_Close->TabStop = false;
			this->Button_Close->Click += gcnew System::EventHandler(this, &MAINFORM::Button_Close_Click);
			// 
			// Button_Menu
			// 
			this->Button_Menu->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Button_Menu.Image")));
			this->Button_Menu->Location = System::Drawing::Point(0, 0);
			this->Button_Menu->Name = L"Button_Menu";
			this->Button_Menu->Size = System::Drawing::Size(47, 47);
			this->Button_Menu->TabIndex = 0;
			this->Button_Menu->TabStop = false;
			this->Button_Menu->Click += gcnew System::EventHandler(this, &MAINFORM::Button_Menu_Click);
			// 
			// Left_Pad
			// 
			this->Left_Pad->BackColor = System::Drawing::Color::Transparent;
			this->Left_Pad->Dock = System::Windows::Forms::DockStyle::Left;
			this->Left_Pad->Location = System::Drawing::Point(0, 150);
			this->Left_Pad->Name = L"Left_Pad";
			this->Left_Pad->Size = System::Drawing::Size(200, 615);
			this->Left_Pad->TabIndex = 1;
			// 
			// Right_Tab
			// 
			this->Right_Tab->BackColor = System::Drawing::Color::Transparent;
			this->Right_Tab->Controls->Add(this->Font_Panel);
			this->Right_Tab->Controls->Add(this->Replace_Panel);
			this->Right_Tab->Dock = System::Windows::Forms::DockStyle::Right;
			this->Right_Tab->Location = System::Drawing::Point(900, 150);
			this->Right_Tab->Name = L"Right_Tab";
			this->Right_Tab->Size = System::Drawing::Size(200, 615);
			this->Right_Tab->TabIndex = 2;
			// 
			// Font_Panel
			// 
			this->Font_Panel->BackgroundImage = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Font_Panel.BackgroundImage")));
			this->Font_Panel->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->Font_Panel->Controls->Add(this->ColPanel_Black);
			this->Font_Panel->Controls->Add(this->ColPanel_Yellow);
			this->Font_Panel->Controls->Add(this->ColPanel_Green);
			this->Font_Panel->Controls->Add(this->ColPanel_Blue);
			this->Font_Panel->Controls->Add(this->label5);
			this->Font_Panel->Controls->Add(this->ColPanel_Red);
			this->Font_Panel->Controls->Add(this->label6);
			this->Font_Panel->Location = System::Drawing::Point(0, 24);
			this->Font_Panel->Name = L"Font_Panel";
			this->Font_Panel->Size = System::Drawing::Size(200, 200);
			this->Font_Panel->TabIndex = 9;
			// 
			// ColPanel_Black
			// 
			this->ColPanel_Black->BackColor = System::Drawing::Color::Black;
			this->ColPanel_Black->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->ColPanel_Black->Location = System::Drawing::Point(125, 154);
			this->ColPanel_Black->Name = L"ColPanel_Black";
			this->ColPanel_Black->Size = System::Drawing::Size(30, 30);
			this->ColPanel_Black->TabIndex = 8;
			this->ColPanel_Black->TabStop = false;
			this->ColPanel_Black->UseVisualStyleBackColor = false;
			this->ColPanel_Black->Click += gcnew System::EventHandler(this, &MAINFORM::ColPanel_Black_Click);
			// 
			// ColPanel_Yellow
			// 
			this->ColPanel_Yellow->BackColor = System::Drawing::Color::Yellow;
			this->ColPanel_Yellow->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->ColPanel_Yellow->Location = System::Drawing::Point(42, 154);
			this->ColPanel_Yellow->Name = L"ColPanel_Yellow";
			this->ColPanel_Yellow->Size = System::Drawing::Size(30, 30);
			this->ColPanel_Yellow->TabIndex = 7;
			this->ColPanel_Yellow->TabStop = false;
			this->ColPanel_Yellow->UseVisualStyleBackColor = false;
			this->ColPanel_Yellow->Click += gcnew System::EventHandler(this, &MAINFORM::ColPanel_Yellow_Click);
			// 
			// ColPanel_Green
			// 
			this->ColPanel_Green->BackColor = System::Drawing::Color::Green;
			this->ColPanel_Green->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->ColPanel_Green->Location = System::Drawing::Point(87, 116);
			this->ColPanel_Green->Name = L"ColPanel_Green";
			this->ColPanel_Green->Size = System::Drawing::Size(30, 30);
			this->ColPanel_Green->TabIndex = 6;
			this->ColPanel_Green->TabStop = false;
			this->ColPanel_Green->UseVisualStyleBackColor = false;
			this->ColPanel_Green->Click += gcnew System::EventHandler(this, &MAINFORM::ColPanel_Green_Click);
			// 
			// ColPanel_Blue
			// 
			this->ColPanel_Blue->BackColor = System::Drawing::Color::Blue;
			this->ColPanel_Blue->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->ColPanel_Blue->Location = System::Drawing::Point(125, 79);
			this->ColPanel_Blue->Name = L"ColPanel_Blue";
			this->ColPanel_Blue->Size = System::Drawing::Size(30, 30);
			this->ColPanel_Blue->TabIndex = 5;
			this->ColPanel_Blue->TabStop = false;
			this->ColPanel_Blue->UseVisualStyleBackColor = false;
			this->ColPanel_Blue->Click += gcnew System::EventHandler(this, &MAINFORM::ColPanel_Blue_Click);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label5->ForeColor = System::Drawing::Color::White;
			this->label5->Location = System::Drawing::Point(17, 49);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(127, 13);
			this->label5->TabIndex = 4;
			this->label5->Text = L"SELECT COLOUR :";
			// 
			// ColPanel_Red
			// 
			this->ColPanel_Red->BackColor = System::Drawing::Color::Red;
			this->ColPanel_Red->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->ColPanel_Red->Location = System::Drawing::Point(42, 79);
			this->ColPanel_Red->Name = L"ColPanel_Red";
			this->ColPanel_Red->Size = System::Drawing::Size(30, 30);
			this->ColPanel_Red->TabIndex = 1;
			this->ColPanel_Red->TabStop = false;
			this->ColPanel_Red->UseVisualStyleBackColor = false;
			this->ColPanel_Red->Click += gcnew System::EventHandler(this, &MAINFORM::ColPanel_Red_Click);
			// 
			// label6
			// 
			this->label6->BackColor = System::Drawing::Color::Silver;
			this->label6->Dock = System::Windows::Forms::DockStyle::Top;
			this->label6->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->label6->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label6->Location = System::Drawing::Point(0, 0);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(200, 23);
			this->label6->TabIndex = 0;
			this->label6->Text = L"FONT COLOUR";
			this->label6->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			this->label6->Click += gcnew System::EventHandler(this, &MAINFORM::label6_Click);
			// 
			// Replace_Panel
			// 
			this->Replace_Panel->BackgroundImage = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Replace_Panel.BackgroundImage")));
			this->Replace_Panel->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->Replace_Panel->Controls->Add(this->label3);
			this->Replace_Panel->Controls->Add(this->label2);
			this->Replace_Panel->Controls->Add(this->Replace_Text);
			this->Replace_Panel->Controls->Add(this->Find_Text);
			this->Replace_Panel->Controls->Add(this->button1);
			this->Replace_Panel->Controls->Add(this->label1);
			this->Replace_Panel->Location = System::Drawing::Point(0, 0);
			this->Replace_Panel->Name = L"Replace_Panel";
			this->Replace_Panel->Size = System::Drawing::Size(200, 30);
			this->Replace_Panel->TabIndex = 8;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label3->ForeColor = System::Drawing::Color::White;
			this->label3->Location = System::Drawing::Point(14, 102);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(119, 13);
			this->label3->TabIndex = 5;
			this->label3->Text = L"REPLACE WITH :";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label2->ForeColor = System::Drawing::Color::White;
			this->label2->Location = System::Drawing::Point(17, 49);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(55, 13);
			this->label2->TabIndex = 4;
			this->label2->Text = L"FIND :";
			// 
			// Replace_Text
			// 
			this->Replace_Text->Location = System::Drawing::Point(17, 121);
			this->Replace_Text->Name = L"Replace_Text";
			this->Replace_Text->Size = System::Drawing::Size(100, 20);
			this->Replace_Text->TabIndex = 3;
			this->Replace_Text->TabStop = false;
			// 
			// Find_Text
			// 
			this->Find_Text->Location = System::Drawing::Point(17, 68);
			this->Find_Text->Name = L"Find_Text";
			this->Find_Text->Size = System::Drawing::Size(100, 20);
			this->Find_Text->TabIndex = 2;
			this->Find_Text->TabStop = false;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(42, 243);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 1;
			this->button1->TabStop = false;
			this->button1->Text = L"REPLACE";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MAINFORM::button1_Click);
			// 
			// label1
			// 
			this->label1->BackColor = System::Drawing::Color::Silver;
			this->label1->Dock = System::Windows::Forms::DockStyle::Top;
			this->label1->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->label1->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(0, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(200, 23);
			this->label1->TabIndex = 0;
			this->label1->Text = L"FIND AND REPLACE";
			this->label1->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			this->label1->Click += gcnew System::EventHandler(this, &MAINFORM::label1_Click);
			// 
			// Bottom_Pad
			// 
			this->Bottom_Pad->BackColor = System::Drawing::Color::Transparent;
			this->Bottom_Pad->Dock = System::Windows::Forms::DockStyle::Bottom;
			this->Bottom_Pad->Location = System::Drawing::Point(200, 735);
			this->Bottom_Pad->Name = L"Bottom_Pad";
			this->Bottom_Pad->Size = System::Drawing::Size(700, 30);
			this->Bottom_Pad->TabIndex = 3;
			// 
			// Menu_Main
			// 
			this->Menu_Main->BackColor = System::Drawing::Color::Gainsboro;
			this->Menu_Main->BackgroundImage = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Menu_Main.BackgroundImage")));
			this->Menu_Main->Controls->Add(this->Label_MenuDescription);
			this->Menu_Main->Controls->Add(this->Button_MenuClose);
			this->Menu_Main->Controls->Add(this->Button_MenuSaveAs);
			this->Menu_Main->Controls->Add(this->Button_MenuSave);
			this->Menu_Main->Controls->Add(this->Button_MenuNew);
			this->Menu_Main->Controls->Add(this->Button_MenuOpen);
			this->Menu_Main->Location = System::Drawing::Point(0, 45);
			this->Menu_Main->Name = L"Menu_Main";
			this->Menu_Main->Size = System::Drawing::Size(390, 280);
			this->Menu_Main->TabIndex = 5;
			this->Menu_Main->Visible = false;
			this->Menu_Main->MouseLeave += gcnew System::EventHandler(this, &MAINFORM::Menu_Main_MouseLeave);
			// 
			// Label_MenuDescription
			// 
			this->Label_MenuDescription->AutoSize = true;
			this->Label_MenuDescription->BackColor = System::Drawing::Color::Transparent;
			this->Label_MenuDescription->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Label_MenuDescription->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->Label_MenuDescription->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 14.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->Label_MenuDescription->ForeColor = System::Drawing::Color::Silver;
			this->Label_MenuDescription->Location = System::Drawing::Point(155, 18);
			this->Label_MenuDescription->Name = L"Label_MenuDescription";
			this->Label_MenuDescription->Size = System::Drawing::Size(220, 202);
			this->Label_MenuDescription->TabIndex = 6;
			this->Label_MenuDescription->Text = L"DE-SCRIBER Word\r\nProcessor.\r\n\r\nDEvelopers Shell\r\nProduction.\r\n\r\nData Structures\r\n" 
				L"Project.\r\n\r\nFALL Semister 2009.";
			// 
			// Button_MenuClose
			// 
			this->Button_MenuClose->BackgroundImage = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Button_MenuClose.BackgroundImage")));
			this->Button_MenuClose->Location = System::Drawing::Point(359, 244);
			this->Button_MenuClose->Name = L"Button_MenuClose";
			this->Button_MenuClose->Size = System::Drawing::Size(16, 16);
			this->Button_MenuClose->TabIndex = 5;
			this->Button_MenuClose->TabStop = false;
			this->Button_MenuClose->UseVisualStyleBackColor = true;
			this->Button_MenuClose->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &MAINFORM::Button_MenuClose_MouseClick);
			// 
			// Button_MenuSaveAs
			// 
			this->Button_MenuSaveAs->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 14.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->Button_MenuSaveAs->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Button_MenuSaveAs.Image")));
			this->Button_MenuSaveAs->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->Button_MenuSaveAs->Location = System::Drawing::Point(13, 177);
			this->Button_MenuSaveAs->Name = L"Button_MenuSaveAs";
			this->Button_MenuSaveAs->Size = System::Drawing::Size(120, 36);
			this->Button_MenuSaveAs->TabIndex = 4;
			this->Button_MenuSaveAs->TabStop = false;
			this->Button_MenuSaveAs->Text = L"SAVE AS";
			this->Button_MenuSaveAs->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->Button_MenuSaveAs->UseVisualStyleBackColor = true;
			// 
			// Button_MenuSave
			// 
			this->Button_MenuSave->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 14.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->Button_MenuSave->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Button_MenuSave.Image")));
			this->Button_MenuSave->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->Button_MenuSave->Location = System::Drawing::Point(13, 122);
			this->Button_MenuSave->Name = L"Button_MenuSave";
			this->Button_MenuSave->Size = System::Drawing::Size(120, 36);
			this->Button_MenuSave->TabIndex = 3;
			this->Button_MenuSave->TabStop = false;
			this->Button_MenuSave->Text = L"SAVE";
			this->Button_MenuSave->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->Button_MenuSave->UseVisualStyleBackColor = true;
			this->Button_MenuSave->Click += gcnew System::EventHandler(this, &MAINFORM::Button_MenuSave_Click);
			// 
			// Button_MenuNew
			// 
			this->Button_MenuNew->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 14.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->Button_MenuNew->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Button_MenuNew.Image")));
			this->Button_MenuNew->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->Button_MenuNew->Location = System::Drawing::Point(13, 17);
			this->Button_MenuNew->Name = L"Button_MenuNew";
			this->Button_MenuNew->Size = System::Drawing::Size(120, 36);
			this->Button_MenuNew->TabIndex = 2;
			this->Button_MenuNew->TabStop = false;
			this->Button_MenuNew->Text = L"CREATE";
			this->Button_MenuNew->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->Button_MenuNew->UseVisualStyleBackColor = true;
			// 
			// Button_MenuOpen
			// 
			this->Button_MenuOpen->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 14.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->Button_MenuOpen->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"Button_MenuOpen.Image")));
			this->Button_MenuOpen->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->Button_MenuOpen->Location = System::Drawing::Point(13, 69);
			this->Button_MenuOpen->Name = L"Button_MenuOpen";
			this->Button_MenuOpen->Size = System::Drawing::Size(120, 36);
			this->Button_MenuOpen->TabIndex = 1;
			this->Button_MenuOpen->TabStop = false;
			this->Button_MenuOpen->Text = L"OPEN";
			this->Button_MenuOpen->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->Button_MenuOpen->UseVisualStyleBackColor = true;
			this->Button_MenuOpen->Click += gcnew System::EventHandler(this, &MAINFORM::Button_MenuOpen_Click);
			// 
			// ProcessManager
			// 
			this->ProcessManager->WorkerReportsProgress = true;
			this->ProcessManager->WorkerSupportsCancellation = true;
			this->ProcessManager->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &MAINFORM::ProcessManager_DoWork);
			this->ProcessManager->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &MAINFORM::ProcessManager_RunWorkerCompleted);
			this->ProcessManager->ProgressChanged += gcnew System::ComponentModel::ProgressChangedEventHandler(this, &MAINFORM::ProcessManager_ProgressChanged);
			// 
			// Main_Panel
			// 
			this->Main_Panel->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->Main_Panel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->Main_Panel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->Main_Panel->Location = System::Drawing::Point(3, 3);
			this->Main_Panel->Name = L"Main_Panel";
			this->Main_Panel->ScrollBars = System::Windows::Forms::RichTextBoxScrollBars::Vertical;
			this->Main_Panel->Size = System::Drawing::Size(686, 553);
			this->Main_Panel->TabIndex = 6;
			this->Main_Panel->Text = L"";
			this->Main_Panel->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MAINFORM::MAINFORM_KeyDown);
			this->Main_Panel->TextChanged += gcnew System::EventHandler(this, &MAINFORM::Main_Panel_TextChanged);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->DefaultExt = L"des";
			this->openFileDialog1->FileName = L"DE-SCRIBER.des";
			this->openFileDialog1->Filter = L"DE-SCRIBER Files (*.des)|*.des";
			this->openFileDialog1->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &MAINFORM::openFileDialog1_FileOk);
			// 
			// saveFileDialog1
			// 
			this->saveFileDialog1->DefaultExt = L"des";
			this->saveFileDialog1->FileName = L"*.des";
			this->saveFileDialog1->Filter = L"DE-SCRIBER Files(*.des)|*.des";
			// 
			// Tab_Panel
			// 
			this->Tab_Panel->Controls->Add(this->tabPage1);
			this->Tab_Panel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->Tab_Panel->Location = System::Drawing::Point(200, 150);
			this->Tab_Panel->Multiline = true;
			this->Tab_Panel->Name = L"Tab_Panel";
			this->Tab_Panel->SelectedIndex = 0;
			this->Tab_Panel->Size = System::Drawing::Size(700, 585);
			this->Tab_Panel->TabIndex = 7;
			this->Tab_Panel->TabStop = false;
			// 
			// tabPage1
			// 
			this->tabPage1->Controls->Add(this->Main_Panel);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(692, 559);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"tabPage1";
			this->tabPage1->UseVisualStyleBackColor = true;
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Interval = 10000;
			this->timer1->Tick += gcnew System::EventHandler(this, &MAINFORM::timer1_Tick);
			// 
			// MAINFORM
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoValidate = System::Windows::Forms::AutoValidate::EnablePreventFocusChange;
			this->BackColor = System::Drawing::SystemColors::Control;
			this->BackgroundImage = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"$this.BackgroundImage")));
			this->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Stretch;
			this->ClientSize = System::Drawing::Size(1100, 765);
			this->Controls->Add(this->Menu_Main);
			this->Controls->Add(this->Tab_Panel);
			this->Controls->Add(this->Bottom_Pad);
			this->Controls->Add(this->Right_Tab);
			this->Controls->Add(this->Left_Pad);
			this->Controls->Add(this->Top_Controls);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->Name = L"MAINFORM";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"MAINFORM";
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MAINFORM::MAINFORM_KeyDown);
			this->Top_Controls->ResumeLayout(false);
			this->Controls_panel1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->Image_Scriber))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->Button_Minimize))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->Button_Close))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->Button_Menu))->EndInit();
			this->Right_Tab->ResumeLayout(false);
			this->Font_Panel->ResumeLayout(false);
			this->Font_Panel->PerformLayout();
			this->Replace_Panel->ResumeLayout(false);
			this->Replace_Panel->PerformLayout();
			this->Menu_Main->ResumeLayout(false);
			this->Menu_Main->PerformLayout();
			this->Tab_Panel->ResumeLayout(false);
			this->tabPage1->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion


private: System::Void MAINFORM_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
			 {
				 if (!this->ProcessManager->IsBusy)
					 this->ProcessManager->RunWorkerAsync();
				 _Manager->Update(this,e,true);
			 }

private: System::Void Button_Close_Click(System::Object^  sender, System::EventArgs^  e)
			 {
				 ProcessManager->CancelAsync();
				 this->Close();
			 }
private: System::Void Button_Minimize_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 this->WindowState = FormWindowState::Minimized;
		 }
private: System::Void Button_Control1_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 MessageBox::Show("CLick");
		 }
private: System::Void Button_Menu_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 if (Menu_Main->Visible == true)
				 Menu_Main->Visible = false;
			 else
				 Menu_Main->Visible = true;
		 }
private: System::Void Menu_Main_MouseLeave(System::Object^  sender, System::EventArgs^  e)
		 {
			Drawing::Rectangle screenBounds(this->PointToScreen(Menu_Main->Location), Menu_Main->Size);
			if (!screenBounds.Contains(MousePosition))
			{
				Menu_Main->Visible = false;
			}
		 }
private: System::Void Button_MenuClose_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
		 {
			 Menu_Main->Visible = false;
			 this->Label_MenuDescription->Text = L"DE-SCRIBER Word\r\nProcessor.\r\n\r\nDEvelopers Shell\r\nProduction.\r\n\r\nData Structures\r\n" 
				L"Project.\r\n\r\nFALL Semister 2009.";
		 }
private: System::Void ProcessManager_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e)
		 {
			 while (true)
			 {
				 if (_Manager->_ProcessQueue.isEmpty())
					 e->Cancel = true;
				 if (ProcessManager->CancellationPending == true)
					 e->Cancel = true;
				 else
					 ProcessManager->ReportProgress(1);
				 Threading::Thread::Sleep(800);
			 }
		 }
private: System::Void ProcessManager_ProgressChanged(System::Object^  sender, System::ComponentModel::ProgressChangedEventArgs^  e)
		 {
			 _Manager->Sync(this);
		 }
private: System::Void ProcessManager_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e)
		 {
			 MessageBox::Show("BuhBYE!!","Message",MessageBoxButtons::OKCancel,MessageBoxIcon::Question);
		 }
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Main_Panel->SelectionColor = Drawing::Color::Red;
		 }
private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 float s = Main_Panel->SelectionFont->Size;
			 if (s<95) s+=4.0;
			 Main_Panel->SelectionFont = gcnew Drawing::Font(Main_Panel->Font->FontFamily->Name,(float)(s));
		 }
private: System::Void button11_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 int Line = Main_Panel->Lines->Length;
			 for (int i=0; i<Line ; i++)
			 {
				 String^ mystr = Main_Panel->Lines[i]->ToString();
				 int ulen = mystr->Length;
				 for (int j=0; j<ulen; j++)
				 {
					 char* ch = new char;
					 //if (mystr
				 }
			 }
		 }
private: System::Void button10_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 if (Main_Panel->SelectedText->Length != 0)
			 {
				 int len = Main_Panel->SelectedText->Length;
				 String^ str = Main_Panel->SelectedText;
				 char* ch = new char[len];
				 for (int i=0; i<len; i++)
					 *(ch+i) = str[i];
				 *(ch+len) = '\0';
				 mTree<char> obj;
				 bool sp = obj.search(ch);
				 if (!sp)
				 {
					 MessageBox::Show("Spell Error");
				 }
			 }
		 }
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Main_Panel->Cut();
		 }
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Main_Panel->Copy();
		 }
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Main_Panel->Paste();
		 }
private: System::Void button8_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 float s = Main_Panel->SelectionFont->Size;
			 if (s >4.0) s-=4.0;
			 Main_Panel->SelectionFont = gcnew Drawing::Font(Main_Panel->Font->FontFamily->Name,(float)(s));
		 }
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Main_Panel->Undo();
		 }
private: System::Void button9_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Main_Panel->Redo();
		 }
private: System::Void pictureBox1_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Replace_Panel->Visible = false;
		 }
private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Font_Panel->Location = Drawing::Point(0,0);
			 Font_Panel->Size = Drawing::Size(200,23);
			 Replace_Panel->Location = Drawing::Point(0,24);
			 System::Drawing::Size^ MySize = Replace_Panel->Size;
			 if (MySize->Height <24)
				 MySize->Height = 320;
			 else
				 MySize->Height = 23;
			 Replace_Panel->Size = Drawing::Size(MySize->Width,MySize->Height);
		 }
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 int i = Main_Panel->Find(Find_Text->Text);
			 if (i>=0)
			 {
			 Main_Panel->Select(i,Find_Text->Text->Length);
			 }
			 Main_Panel->Select(0,5);
			 Main_Panel->SelectedText->Replace("hello 1234","heyy");//Find_Text->Text,Replace_Text->Text);
		 }
private: System::Void Button_Control1_Click_1(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void Button_MenuOpen_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 openFileDialog1->ShowDialog();
		 }
private: System::Void openFileDialog1_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e)
		 {
			 MessageBox::Show(openFileDialog1->FileName);
		 }
private: System::Void Button_MenuSave_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 saveFileDialog1->ShowDialog();
			 MessageBox::Show(saveFileDialog1->FileName);
		 }
private: System::Void label6_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Replace_Panel->Location = Drawing::Point(0,0);
			 Replace_Panel->Size = Drawing::Size(200,23);
			 Font_Panel->Location = Drawing::Point(0,24);
			 System::Drawing::Size^ MySize = Font_Panel->Size;
			 if (MySize->Height <24)
				 MySize->Height = 200;
			 else
				 MySize->Height = 23;
			 Font_Panel->Size = Drawing::Size(MySize->Width,MySize->Height);
		 }
private: System::Void ColPanel_Red_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Main_Panel->SelectionColor = Drawing::Color::Red;
		 }
private: System::Void ColPanel_Blue_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Main_Panel->SelectionColor = Drawing::Color::Blue;
		 }
private: System::Void ColPanel_Green_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Main_Panel->SelectionColor = Drawing::Color::Green;
		 }
private: System::Void ColPanel_Black_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Main_Panel->SelectionColor = Drawing::Color::Black;
		 }
private: System::Void ColPanel_Yellow_Click(System::Object^  sender, System::EventArgs^  e)
		 {
			 Main_Panel->SelectionColor = Drawing::Color::Yellow;
		 }
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e)
		 {
			 if (!_Manager->_Doc_State.isSaved)
				_Manager->_Doc_State.AutoStore(Main_Panel);
		 }
private: System::Void button12_Click(System::Object^  sender, System::EventArgs^  e)
		 {
		 }
private: System::Void Main_Panel_TextChanged(System::Object^  sender, System::EventArgs^  e)
		 {
			 _Manager->_Doc_State.isSaved = false;
		 }
};
}
