#pragma once

using namespace System;
using namespace System::Windows::Forms;

namespace DESCRIBER{

class Cursor
{

#pragma region Class Members
private:
	char* _LblID;
	char* _Word;
	unsigned int _Col;
	unsigned int _FontSize;

#pragma endregion


public:

	Cursor(void)
	{
		_LblID = NULL;
		_Word = NULL;
	}
	unsigned int FontSize()
	{
		return _FontSize;
	}
	void FontSize(unsigned int p)
	{
		_FontSize = p;
	}
	unsigned int Col()
	{
		return _Col;
	}
	void Col(unsigned int p)
	{
		_Col = p;
	}
	Cursor& operator=(Cursor& rhs)
	{
		if (this == &rhs)
			return *this;
		else
		{
			_LblID = "\0";
			_Word = "\0";
			_LblID = rhs._LblID;
			_Word = rhs._Word;
		}
		return (*this);
	}
	void AssignLabel(char* p)
	{
		int len = strlen(_LblID);
		p = new char [len];
		for (Byte i=0; i<len; i++)

			*(p+i) = *(_Word+i);
		*(p+len) = '\0';
	}
	void AssignWord(char* p)
	{
		int len = strlen(_Word);
		//p = new char [len];
		for (Byte i=0; i<len; i++)

			*(p+i) = *(_Word+i);
		*(p+len) = '\0';
	}
	char* Word()
	{
		return _Word;
	}
	char* LblID()
	{
		return _LblID;
	}
	bool isEmpty()
	{
		return ( (strlen(_Word))?true:false );
	}
	void AddChar(Form^ MyForm, char input)//type input
	{
		char* ch;
		int len = 1;
		if (_Word!=NULL)
		{
			len += strlen(_Word);
		ch = new char [len];
		for (Byte i=0; i<strlen(_Word); i++)
			*(ch+i) = *(_Word+i);
		*(ch+strlen(_Word)) = input;
		*(ch+len) = '\0';
		}
		else
		{
			ch = new char[len];
			*(ch+0) = input;
			*(ch+1) = '\0';
		}
		//delete _Word;
		//_Word = NULL;
		_Word = new char [strlen(ch)];
		for (Byte i=0; i<strlen(ch); i++)
			*(_Word+i) = *(ch+i);
		*(_Word+strlen(ch)) = '\0';
	}
	char* LabelID(int p[])
	{
		for (Byte i=0; i<2; i++)
		{
			p[0] *= 10;
			p[0] += (int) (*(_LblID+i));
		}
		for (Byte i=2; i<4; i++)
		{
			p[1] *= 10;
			p[1] += (int) (*(_LblID+i));
		}
		for (Byte i=4; i<6; i++)
		{
			p[2] *= 10;
			p[2] += (int) (*(_LblID+i));
		}
		return _LblID;
	}
	void RemoveChar()//backspace
	{
	}
};

};