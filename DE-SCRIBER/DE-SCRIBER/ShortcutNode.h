struct ShortcutNode{
           unsigned int keycode ;
           unsigned int KeyAscii;
           bool operator == (const ShortcutNode & rhs)
           {
                if (rhs.KeyAscii == KeyAscii)
                   return 1;
                return 0;
           }
           bool operator == ( unsigned int rhs)
           {
                if (rhs == KeyAscii)
                   return 1;
                return 0;
           }
           ShortcutNode()
           {
               keycode  =0;
               KeyAscii =0;            
           }
           ShortcutNode(unsigned int kc ,unsigned int ka )
           {
                   keycode = kc ;
                   KeyAscii = ka;
           }
           ShortcutNode(const ShortcutNode& rhs)
           {
                  keycode = rhs.keycode ;
                  KeyAscii = rhs.KeyAscii;
           }
           ShortcutNode& operator = (const ShortcutNode & rhs)
           {
                     if (*this == rhs)
                        return *this ;
                      this->keycode = rhs.keycode ;
                      this->KeyAscii = rhs.KeyAscii;
                      return  *this;
           }
};