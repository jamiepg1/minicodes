#include <iostream>

using namespace std;

struct Dim{
       unsigned int x;
       unsigned int y;
       Dim(unsigned int Px, unsigned int Py)
       {
                    x = Px;
                    y = Py;
       }
       };

template <class T>
class VisualDoublyLinkedList;

template <class T>
class VDNode{

      private:
              friend class VisualDoublyLinkedList<T>;
              T _Data;
              VDNode<T> *_Next;
              VDNode<T> *_Prev;
              char* _LblID;
              Dim* _Start;
              Dim* _Dimension;

      public:
             VDNode():_Data(0),_Next(NULL),_Prev(NULL),_LblID(NULL),_Start(NULL),_Dimension(NULL){}
             VDNode(const T& d):_Next(NULL),_Prev(NULL),_LblID(NULL),_Start(NULL),_Dimension(NULL)
             {
                        _Data=d;
             }
             VDNode(const T& d, VDNode<T> *Aptr, VDNode<T> *Bptr):_LblID(NULL),_Start(NULL),_Dimension(NULL)
             {
                        _Data = d;
                        _Next = Aptr;
                        _Prev = Bptr;
             }
             VDNode(const T& d, VDNode<T> *Aptr, VDNode<T> *Bptr, char* ID, unsigned int* St, unsigned int* Di)
             {
                        _Data = d;
                        _Next = Aptr;
                        _Prev = Bptr;
                        _LblID = ID;
                        _Start = new Dim(St[0],St[1]);
                        _Dimension = new Dim(Di[0],Di[1]);
             }
             VDNode(const T& d, char* Lbl, unsigned int PStartx, unsigned int PStarty, unsigned int Dimx, unsigned int Dimy):_Next(NULL),_Prev(NULL)
             {
                        _Data = d;
                        _LblID = Lbl;
                        _Start = new Dim(PStartx,PStarty);
                        _Dimension = new Dim(Dimx,Dimy);
             }
             VDNode(const VDNode<T>& rhs):_Data(0),_Next(NULL),_Prev(NULL),_LblID(NULL),_Start(NULL),_Dimension(NULL)
             {
                        this->_Data = rhs._Data;
                        this->_Next = rhs._Next;
                        this->_Prev = rhs._Prev;
                        this->_LblID = rhs._LblID;
                        this->_Start = new Dim(0,0);
                        unsigned int p[2] = {rhs._Start->x, rhs._Start->y};
                        this->Start(p);
                        this->_Dimension = new Dim(0,0);
                        unsigned int p1[2] = {rhs._Dimension->x, rhs._Dimension->y};
                        this->Dimension(p1);
             }
             ~VDNode()
             {
                    _Data = 0;
                    _Next = NULL;
                    _Prev = NULL;
                    _LblID = NULL;
                    delete _Start;
                    _Start = NULL;
                    delete _Dimension;
                    _Dimension = NULL;
             }
             T Data(){return _Data;}
             void Data(T d){_Data = d;}
			 VDNode<T>* Next(){return _Next;}
			 VDNode<T>* Prev(){return _Prev;}
             char* LblID(){return _LblID;}
             void LblID(char* id){_LblID = id;}
             void getStart(unsigned int p[])
             {
                   p[0] = _Start->x;
                   p[1] = _Start->y;
             }
             void Start(unsigned int* a)
             {
                   _Start->x = a[0];
                   _Start->y = a[1];
             }
             void getDimension(unsigned int p[])
             {
                   p[0] = _Dimension->x;
                   p[1] = _Dimension->y;
             }
             void Dimension(unsigned int* a)
             {
                   _Dimension->x = a[0];
                   _Dimension->y = a[1];
             }
      };

template <class T>
class VisualDoublyLinkedList{
      
      private:
              VDNode<T> *Head;
              unsigned int _Size;
              unsigned int _MaxHeight;
      public:
             VisualDoublyLinkedList():Head(NULL),_Size(0),_MaxHeight(0){}
             VisualDoublyLinkedList(VisualDoublyLinkedList & rhs);
             void InsertAtLast(const T& d);
             void InsertAtLast(const T& d, char* Lbl, unsigned int Stx, unsigned int Sty, unsigned int Dix, unsigned int Diy);
             void Insert(unsigned int W, const T& d, char* Lbl, unsigned int Stx, unsigned int Sty, unsigned int Dix, unsigned int Diy);
             void InsertAtFront(const T& d);
             void InsertAtFront(const T& d, char* Lbl, unsigned int Stx, unsigned int Sty, unsigned int Dix, unsigned int Diy);
             void RemoveAtLast();
             void RemoveAtFront();
             void DeleteList();
             unsigned int Size(){return _Size;}
             VDNode<T>* getHead(){return Head;}
             VisualDoublyLinkedList<T>& operator = (VisualDoublyLinkedList<T>& rhs);
             void Print();
             void PrintReverse();
             /*void Update(T *new ,  int Wordnumber )
             T getlabel // it will return the text of data 
            void delete (Int VDNodeID)  */
      };
      
template <class T>
VisualDoublyLinkedList<T>& VisualDoublyLinkedList<T>::operator = (VisualDoublyLinkedList<T> & rhs)
{
    if (this->Size()>0)
       this->DeleteList();
    if (rhs.Size()!=0)
    {
         this->_Size = rhs.Size();
         VDNode<T> *Temp = rhs.Head;
         VDNode<T> *Temp2 = new VDNode<T>(*(rhs.getHead()));
         this->Head = Temp2;
         Temp = Temp->_Next;
         while (Temp!=NULL)
         {
               unsigned int p[2]; Temp->getStart(p);
               unsigned int p1[2]; Temp->getDimension(p1);
               Temp2->_Next = new VDNode<T> (Temp->Data(),NULL,Temp2,Temp->LblID(),p,p1);
               Temp2 = Temp2->_Next;
               Temp = Temp->_Next;
         }
         Temp2->_Next = NULL;
         return (*this);
    }
}

      
template <class T>
void VisualDoublyLinkedList<T>::DeleteList()
{
     if (Head!=NULL)
     {
        VDNode<T> *Temp= Head;
        VDNode<T> *Temp2;
        while (Temp!=NULL)
        {
              Temp2 = Temp;
              Temp = Temp->_Next;
              Temp2->_Prev = NULL;
              Temp2->_Next = NULL;
              delete Temp2;
        }
        Head = NULL;
     }
}


template <class T>
VisualDoublyLinkedList<T>::VisualDoublyLinkedList(VisualDoublyLinkedList<T>& rhs):Head(NULL),_Size(0)
{
    if (rhs.Size()!=0)
    {
          _Size = rhs.Size();
          VDNode<T> *Temp = rhs.Head;
          VDNode<T> *TempO;
          TempO = new VDNode<T> (*(rhs.Head));
          Head = TempO;
          Temp = Temp->_Next;
          while (Temp!=NULL)
          {
                unsigned int p[2]; Temp->getStart(p);
                unsigned int p1[2]; Temp->getDimension(p1);
                TempO->_Next = new VDNode<T> (Temp->Data(),NULL,TempO,Temp->LblID(),p,p1);
                TempO = TempO->_Next;
                Temp = Temp->_Next;
          }
    }
}


template <class T>
void VisualDoublyLinkedList<T>::RemoveAtLast()
{
     if (Head!=NULL)
     {
        VDNode<T> *Temp = Head;
        
        while( (Temp->_Next!=NULL) && (Temp->_Next->_Next!=NULL) )
             Temp = Temp->_Next;
        if ( (Temp == Head) && (_Size<2) )
        {
                 delete Temp;
                 Head = NULL;
        }
        else
        {
                 delete Temp->_Next;
                 Temp->_Next = NULL;
        }
     _Size--;
     }
}


template <class T>
void VisualDoublyLinkedList<T>::RemoveAtFront()
{
     if (Head!=NULL)
     {
        VDNode<T> *Temp = Head->_Next;
        Head->_Next = NULL;
        delete Head;
        Head = Temp;
        Head->_Prev = NULL;
        _Size--;
     }
}
      
template <class T>
void VisualDoublyLinkedList<T>::InsertAtLast(const T& d)
{
     VDNode<T> *Temp=Head;
     
     if (Head==NULL)
     {
        Temp = new VDNode<T>(d);
        Head = Temp;
     }
     else
     {
         while (Temp->_Next!=NULL)
           Temp = Temp->_Next;
         Temp->_Next = new VDNode<T>(d,NULL,Temp);
     }
     _Size++;
}


template <class T>
void VisualDoublyLinkedList<T>::InsertAtLast(const T& d, char* Lbl, unsigned int Stx, unsigned int Sty, unsigned int Dix, unsigned int Diy)
{
     VDNode<T> *Temp=Head;
     
     if (Head==NULL)
     {
        Temp = new VDNode<T>(d, Lbl, Stx, Sty, Dix, Diy);
        Head = Temp;
     }
     else
     {
         while (Temp->_Next!=NULL)
           Temp = Temp->_Next;
         unsigned int p[2] = {Stx,Sty};
         unsigned int p1[2] = {Dix,Diy};
         Temp->_Next = new VDNode<T>(d, NULL, Temp, Lbl, p, p1);
     }
     _Size++;
}

template <class T>
void VisualDoublyLinkedList<T>::Insert(unsigned int W, const T& d, char* Lbl, unsigned int Stx, unsigned int Sty, unsigned int Dix, unsigned int Diy)
{
     if(W > 0)
     {
     if ( (this->Size() < W-1) && (W!=1) )
		this->InsertAtLast(d, Lbl, Stx, Sty, Dix, Diy);
	else
	{
     VDNode<T> *Temp=Head;
     
     if (W==1)
     {
        InsertAtFront(d,Lbl,Stx,Sty,Dix,Diy);
     }
     else
     {
         W--;
         while (W!=0)
         {
               Temp = Temp->_Next;
               W--;
         }
         unsigned int p[2] = {Stx,Sty};
         unsigned int p1[2] = {Dix,Diy};
         Temp->_Next = new VDNode<T>(d, NULL, Temp, Lbl, p, p1);
         _Size++;
     }
     }
     }
}

template <class T>
void VisualDoublyLinkedList<T>::InsertAtFront(const T& d)
{
     VDNode<T> *Temp = new VDNode<T>(d);
     assert(Temp!=0);
     
     Temp->_Next = Head;
     if (Head!=NULL)
     Head->_Prev = Temp;
     Head = Temp;
     _Size++;
}


template <class T>
void VisualDoublyLinkedList<T>::InsertAtFront(const T& d, char* Lbl, unsigned int Stx, unsigned int Sty, unsigned int Dix, unsigned int Diy)
{
     VDNode<T> *Temp = new VDNode<T>(d, Lbl, Stx, Sty, Dix, Diy);
     //assert(Temp!=0);
     
     Temp->_Next = Head;
     if (Head!=NULL)
     Head->_Prev = Temp;
     Head = Temp;
     _Size++;
}


template<class T>
void VisualDoublyLinkedList<T>::Print()
{
     VDNode<T> *Temp=Head;
     
     while (Temp!=NULL)
     {
           cout<<Temp->Data()<<" : ";
           cout<<Temp->LblID()<<" : ";
           unsigned int p[2];
           Temp->getStart(p);
           cout<<p[0]<<"  "<<p[1]<<" : ";
           unsigned int q[2];
           Temp->getDimension(q);
           cout<<q[0]<<"  "<<q[1]<<"  ";
           cout<<endl;
           
           Temp = Temp->_Next;
     }
     cout<<endl;
}

template<class T>
void VisualDoublyLinkedList<T>::PrintReverse()
{
     VDNode<T> *Temp=Head;
     
     while (Temp->_Next!=NULL)
           Temp = Temp->_Next;
     do{
           cout<<Temp->Data()<<" ";
           Temp = Temp->_Prev;
     }while (Temp!=NULL);
     cout<<endl;
}
