#include <iostream>

using namespace std;

template <class T>
class DoublyLinkedList;

template <class T>
class DNode{

      private:
              friend class DoublyLinkedList<T>;
              T _Data;
              DNode<T> *_Next;
              DNode<T> *_Prev;
              bool _Sp_Error;
              int _FontSize;
              int _Colour;

      public:
             DNode():_Data(0),_Next(NULL),_Prev(NULL),_Sp_Error(0),_FontSize(0),_Colour(0){}
			 DNode(const DNode<T>& rhs):_Data(0),_Next(NULL),_Prev(NULL),_Sp_Error(0),_FontSize(0),_Colour(0)
			 {
                         this->_Data = rhs._Data;
                         this->_Next = rhs._Next;
                         this->_Prev = rhs._Prev;
                         this->_Sp_Error = rhs._Sp_Error;
                         this->_FontSize = rhs._FontSize;
                         this->_Colour = rhs._Colour;
             }
             DNode(const T& d):_Next(NULL),_Prev(NULL),_Sp_Error(0),_FontSize(0),_Colour(0)
             {
                        _Data=d;
             }
             DNode(const T& d, DNode<T> *Aptr, DNode<T> *Bptr):_Sp_Error(0),_FontSize(0),_Colour(0)
             {
                        _Data = d;
                        _Next = Aptr;
                        _Prev = Bptr;
             }
             DNode(const T d, DNode<T> *Aptr, DNode<T> *Bptr, bool Sp, int FSize, int Col)
             {
                        _Data = new char[strlen(d)];
						Byte i;
						for (i=0; i<strlen(d); i++)
							*(_Data+i) = *(d+i);
						*(_Data+i) = '\0';
						//_Data = d;
                        _Next = Aptr;
                        _Prev = Bptr;
                        _Sp_Error = Sp;
                        _FontSize = FSize;
                        _Colour = Col;
             }
             DNode(const T& d, int FSize, int Col):_Next(NULL),_Prev(NULL),_Sp_Error(0)
             {
                        _Data = d;
                        _FontSize = FSize;
                        _Colour = Col;
             }
             ~DNode()
             {
                    _Data = 0;
                    _Next = NULL;
                    _Prev = NULL;
                    _Sp_Error = false;
                    _FontSize = 0;
                    _Colour = 0;
             }
             T Data(){return _Data;}
             void Data(T d){_Data = d;}
             DNode<T>* Next(){return _Next;}
			 DNode<T>* Prev(){return _Prev;}
             bool Sp_Error(){return _Sp_Error;}
             void Sp_Error(bool sp){_Sp_Error = sp;}
             int FontSize(){return _FontSize;}
             void FontSize(T size){_FontSize = size;}
             int Colour(){return _Colour;}
             void Colour(T col){_Colour = col;}
      };

template <class T>
class DoublyLinkedList{
      
      private:
              DNode<T> *Head;
              unsigned _Size;
      public:
             DoublyLinkedList():Head(NULL),_Size(0){}
             DoublyLinkedList(DoublyLinkedList & rhs);
             void InsertAtLast(const T& d);
             void InsertAtLast(const T d, bool Sp, int FSize, int Col);
             void Insert(unsigned int W, const T& d, bool Sp, int FSize, int Col);
             void InsertAtFront(const T& d);
             void InsertAtFront(const T& d, bool Sp, int FSize, int Col);
             void RemoveAtLast();
             void RemoveAtFront();
             void DeleteList();
             unsigned int Size(){return _Size;}
             DNode<T>* getHead(){return Head;}
             DoublyLinkedList<T>& operator = (DoublyLinkedList<T>& rhs);
             DNode<T>* getNode(int No);
             void Print();
             void PrintReverse();
             /*void Update(T *new ,  int Wordnumber )
             T getlabel // it will return the text of data 
            void delete (Int DNodeID)  */
      };
      
template <class T>
DNode<T>* DoublyLinkedList<T>::getNode(int No)
{
       if ( (No > 0) && (No <= _Size) )
       {
              DNode<T> *Temp = getHead();
              if (Temp != NULL)
              while (No-- > 1)
                    Temp = Temp->_Next;
              
              return Temp;
       }
}


template <class T>
DoublyLinkedList<T>& DoublyLinkedList<T>::operator = (DoublyLinkedList<T> & rhs)
{
    if (this->Size()>0)
       this->DeleteList();
    if (rhs.Size()!=0)
    {
         this->_Size = rhs.Size();
         DNode<T> *Temp = rhs.Head;
         DNode<T> *Temp2 = new DNode<T>(*(rhs.Head));
         this->Head = Temp2;
         Temp = Temp->_Next;
         while (Temp!=NULL)
         {
               Temp2->_Next = new DNode<T> (Temp->Data(),NULL,Temp2,Temp->Sp_Error(),Temp->FontSize(),Temp->Colour());
               Temp2 = Temp2->_Next;
               Temp = Temp->_Next;
         }
         Temp2->_Next = NULL;
         return (*this);
    }
}

      
template <class T>
void DoublyLinkedList<T>::DeleteList()
{
     if (Head!=NULL)
     {
        DNode<T> *Temp= Head;
        DNode<T> *Temp2;
        while (Temp!=NULL)
        {
              Temp2 = Temp;
              Temp = Temp->_Next;
              Temp2->_Prev = NULL;
              Temp2->_Next = NULL;
              delete Temp2;
        }
        Head = NULL;
     }
	 _Size = 0;
}


template <class T>
DoublyLinkedList<T>::DoublyLinkedList(DoublyLinkedList<T>& rhs):Head(NULL),_Size(0)
{
    if (rhs.Size()!=0)
    {
          _Size = rhs.Size();
          DNode<T> *Temp =rhs.Head;
          DNode<T> *TempO;
          TempO = new DNode<T> (*(rhs.Head));
          Head = TempO;
          Temp = Temp->_Next;
          while (Temp!=NULL)
          {
                TempO->_Next = new DNode<T> (Temp->Data(),NULL,TempO,Temp->Sp_Error(),Temp->FontSize(),Temp->Colour());
                TempO = TempO->_Next;
                Temp = Temp->_Next;
          }
    }
}


template <class T>
void DoublyLinkedList<T>::RemoveAtLast()
{
     if (Head!=NULL)
     {
        DNode<T> *Temp = Head;
        
        while( (Temp->_Next!=NULL) && (Temp->_Next->_Next!=NULL) )
             Temp = Temp->_Next;
        if ( (Temp == Head) && (_Size<2) )
        {
                 delete Temp;
                 Head = NULL;
        }
        else
        {
                 delete Temp->_Next;
                 Temp->_Next = NULL;
        }
     _Size--;
     }
}


template <class T>
void DoublyLinkedList<T>::RemoveAtFront()
{
     if (Head!=NULL)
     {
        DNode<T> *Temp = Head->_Next;
        Head->_Next = NULL;
        delete Head;
        Head = Temp;
        Head->_Prev = NULL;
        _Size--;
     }
}
      
template <class T>
void DoublyLinkedList<T>::InsertAtLast(const T& d)
{
     DNode<T> *Temp=Head;
     
     if (Head==NULL)
     {
        Temp = new DNode<T>(d);
        Head = Temp;
     }
     else
     {
         while (Temp->_Next!=NULL)
           Temp = Temp->_Next;
         Temp->_Next = new DNode<T>(d,NULL,Temp);
     }
     _Size++;
}


template <class T>
void DoublyLinkedList<T>::InsertAtLast(const T d, bool Sp, int FSize, int Col)
{
     DNode<T>* Temp=Head;
     
     if (Head==NULL)
     {
        Temp = new DNode<T>(d,NULL,NULL,Sp,FSize,Col);
        Head = Temp;
     }
     else
     {
         while (Temp->_Next!=NULL)
           Temp = Temp->_Next;
         Temp->_Next = new DNode<T>(d,NULL,Temp,Sp,FSize,Col);
	 }
     _Size++;
}


template <class T>
void DoublyLinkedList<T>::Insert(unsigned int W, const T& d, bool Sp, int FSize, int Col)
{
    if (W > 0)
    {
	if ( (this->Size() < W-1) && (W!=1) )
		InsertAtLast(d, Sp, FSize, Col);
	else
	{
		DNode<T>* Temp;
		Temp = Head;
     
		if (W==1)
		{
              InsertAtFront(d, Sp, FSize, Col);
		}
		else
		{
			W -= 2;
			while (W!=0)
			{
               Temp = Temp->_Next;
               W--;
			}
			Temp->_Next = new DNode<T>(d,NULL,Temp,Sp,FSize,Col);
			_Size++;
		}
	}
   }
}

template <class T>
void DoublyLinkedList<T>::InsertAtFront(const T& d, bool Sp, int FSize, int Col)
{
     DNode<T> *Temp = new DNode<T>(d,NULL,NULL,Sp,FSize,Col);
     //assert(Temp!=0);
     
     Temp->_Next = Head;
     if (Head!=NULL)
     Head->_Prev = Temp;
     Head = Temp;
     _Size++;
}

template <class T>
void DoublyLinkedList<T>::InsertAtFront(const T& d)
{
     DNode<T> *Temp = new DNode<T>(d);
     assert(Temp!=0);
     
     Temp->_Next = Head;
     if (Head!=NULL)
     Head->_Prev = Temp;
     Head = Temp;
     _Size++;
}

template<class T>
void DoublyLinkedList<T>::Print()
{
     DNode<T> *Temp=Head;
     
     while (Temp!=NULL)
     {
           cout<<Temp->Data()<<" : ";
           cout<<Temp->Sp_Error()<<" : ";
           cout<<Temp->FontSize()<<" : ";
           cout<<Temp->Colour();
           cout<<endl;
           Temp = Temp->_Next;
     }
     cout<<endl;
}

template<class T>
void DoublyLinkedList<T>::PrintReverse()
{
     DNode<T> *Temp=Head;
     
     while (Temp->_Next!=NULL)
           Temp = Temp->_Next;
     do{
           cout<<Temp->Data()<<" ";
           Temp = Temp->_Prev;
     }while (Temp!=NULL);
     cout<<endl;
}
