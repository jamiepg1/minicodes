#pragma once
#include "State.h"
#include "ProQueue.h"
#include "Shortcut.h"


using namespace System;
using namespace System::Windows::Forms;

namespace DESCRIBER{

struct FuncNode
{

#pragma region Class Members
	unsigned int _Priority;
	unsigned int MyKey; // parameter at times
	unsigned int KeyCode;
	union Function{
		void (State::*funcchar) (Form^,char); //input
		void (State::*functextbox) (RichTextBox^); //save---replace--find---. inc--- , dec
		void (State::*funcform) (Form^); // tab--open---new---save n clos--
		}MyFunc;


#pragma endregion

public:
	FuncNode(void)
	{
	}
};



class Controller
{

#pragma region Class Members
private:
	Shortcut _MyShortcut;
public: 
	State _Doc_State;
	ProQueue<FuncNode> _ProcessQueue;
	bool isLoad;
	bool isOpen;
#pragma endregion


public:

	Controller(): isLoad(false),isOpen(false)
	{
	}

	void SetDocADDR(char* str)
	{
		int len = strlen(str);
		//_Doc_State._Doc_ADDR = new char [len];
		for (Byte i=0; i<len;i++)
			*(_Doc_State._Doc_ADDR+i) = *(str+i);
		*(_Doc_State._Doc_ADDR+len) = '\0';
	}

	void StartUp(Form^ MyForm)
	{
		if (isOpen)
		{
			_Doc_State.OpenDoc(MyForm);
		}
		//_Doc_State._MyVisual.StartUp(MyForm);
		if (isLoad)
		{
			_Doc_State.StartUp(((RichTextBox^) ((TabControl^)(MyForm->Controls["Tab_Panel"]))->TabPages[0]->Controls["Main_Panel"]));
		}
	}

	void Update(System::Windows::Forms::Form^ MAIN, System::Windows::Forms::KeyEventArgs^  key, System::Boolean IsKey)
	{
		/*if ( MAIN->Controls->ContainsKey("Meu_Main") )
			MAIN->Controls["Menu_Main"]->Visible = true;
		if ( !MAIN->Controls->ContainsKey("Label1") )
		{
		Label ^NewLbl = gcnew Label();
		NewLbl->Name = "Label1";
		NewLbl->Location = System::Drawing::Point(40,50);
		NewLbl->AutoSize = true;
		//NewLbl->BackColor = System::Drawing::Color::Transparent;
		MAIN->Controls["WordPanel"]->Controls->Add(NewLbl);
		}*/

		if (IsKey)
			RecogKey(key);

		//MAIN->Controls["WordPanel"]->Controls["Label1"]->Text += Convert::ToChar(ch);
		//MessageBox::Show("hello","Heyyy",MessageBoxButtons::OK,MessageBoxIcon::Warning);
		//RecogKey()
		//Add to Process Queue!
	}

	void RecogKey(System::Windows::Forms::KeyEventArgs^  key)
	{
		unsigned int code[2] = {key->KeyValue,0};
		_MyShortcut.getCode(code);//Ctrl + S ==22
		bool AllowRepeat;
		FuncNode FN;
		FN.MyKey = key->KeyValue;
		FN._Priority = 0;
		FN.KeyCode = code[1];

		/*switch (code[1]){
			case 1 : FN.MyFunc.funcchar = &State::AddToCursor;
					AllowRepeat = true;
					FN._Priority = 1;			
						break;
			case 0 : break;
		};*/

		#pragma region Key Switch
		switch (code[1]){
			case 1:
				FN.MyFunc.funcform = & State::SwitchTAB;
				AllowRepeat = false;
				FN._Priority = 1;
					break;
           case 2:
			   FN.MyFunc.functextbox = & State::Find;
			   AllowRepeat = false;
			   FN._Priority = 1;
				break;
		   case 3:
			   FN.MyFunc.functextbox = &State::FindNReplace;
			   AllowRepeat = false;
			   FN._Priority = 1;
				break;
		   case 4:
				FN.MyFunc.funcform = & State::NewDoc;
				AllowRepeat = false;
				FN._Priority = 1;
					break;
		   case 5:
				FN.MyFunc.funcform = & State::OpenDoc;
				AllowRepeat = false;
				FN._Priority = 1;
					break;
		   case 6: 
			    FN.MyFunc.functextbox = &State::SaveNow;
				AllowRepeat = false;
				FN._Priority = 3;
					break;
           case 7:
			   FN.MyFunc.funcform = & State::SavenClose;
				AllowRepeat = false;
				FN._Priority = 1;
					break;
           case 8:
				FN.MyFunc.functextbox = & State::FontSizeDec;
				AllowRepeat = false;
				FN._Priority = 1;
					break;
           case 9:
				FN.MyFunc.functextbox = & State::FontSizeInc;
				AllowRepeat = false;
				FN._Priority = 1;
					break;
		   case 10://input..
			   break;
          };

		#pragma endregion


		if (FN._Priority!=0)
		_ProcessQueue.enQueue(FN,FN._Priority,AllowRepeat);
	}

	bool Sync(Form^ Main)
	{
			if (! _ProcessQueue.isEmpty() )
			{
				FuncNode FN = _ProcessQueue.deQueue();

				switch (FN.KeyCode){
					//case 1 : (_Doc_State.*(FN.MyFunc.funcchar)) (Main,Convert::ToChar(FN.MyKey));
					//	break;//input to cursor
					case 5 :
						break;//if (
					case 6 : 
						if (strlen(_Doc_State._Doc_ADDR) == 0)
						{
							SaveFileDialog^ obj = gcnew SaveFileDialog();
							obj->Filter = "DE-SCRIBER Files(*.des)|*.des";
							obj->ShowDialog();
							if (obj->FileName != "")
							{
								char ch[255];
								for (int i=0;i<255;i++)
									ch[i] = '\0';
								int len = obj->FileName->Length;
								String^ str = obj->FileName;
								for (int i=0; i<len; i++)
									ch[i] = str[i];
								SetDocADDR(ch);
								(_Doc_State.*(FN.MyFunc.functextbox)) ((RichTextBox^) ((TabControl^)(Main->Controls["Tab_Panel"]))->TabPages[0]->Controls["Main_Panel"]);
							}
						}
						else
							(_Doc_State.*(FN.MyFunc.functextbox)) ((RichTextBox^) ((TabControl^)(Main->Controls["Tab_Panel"]))->TabPages[0]->Controls["Main_Panel"]);
						break;
					case 8 :
						(_Doc_State.*(FN.MyFunc.functextbox)) ((RichTextBox^) ((TabControl^)(Main->Controls["Tab_Panel"]))->TabPages[0]->Controls["Main_Panel"]);
						break;
					case 9 :
						(_Doc_State.*(FN.MyFunc.functextbox)) ((RichTextBox^) ((TabControl^)(Main->Controls["Tab_Panel"]))->TabPages[0]->Controls["Main_Panel"]);
						break;
				};
				return true;
			}
			else
				return false;
	}
};

};

