#pragma once

namespace DESCRIBER{

class Chunk
{

#pragma region Class Members
public:
	DoublyLinkedList<char*>* _MyLine;
	unsigned int _CurLine;
	unsigned int MAX_Line;

#pragma endregion


public:

	Chunk(void)
	{
		_CurLine = 0;
		MAX_Line = 20;
		_MyLine = new DoublyLinkedList<char*> [MAX_Line];
	}

	unsigned int CurLine()
	{
		return _CurLine;
	}
	void CurLine(unsigned int p)
	{
		_CurLine = p;
	}

	void AddWordToDLL(unsigned int Line, unsigned int Word, char* ch, bool sp, unsigned int size, unsigned int col)
	{
		if ( Line < MAX_Line )
			_MyLine[Line].InsertAtLast(ch,sp,size,col);
	}

};

};
