#pragma once
#ifndef INCLUDED_SLL
#include "SLL.h"
#define INCLUDED_SLL true
#endif

template <class T>
class ProQueue{
      
      private:
              SinglyLinkedList<T> Data;
              unsigned int Size;
      public:
			static int MAXINT;
             ProQueue(){Size = MAXINT;}
             ProQueue(unsigned int s){Size = s;}
             bool isEmpty();
             bool isFull();
             void Clear();
             T deQueue();
             void enQueue(T el);
			 void enQueue(T el, unsigned int PR, bool AllowRepeat);
             T First();
      
      };
      
template <class T>
int ProQueue<T>::MAXINT = 5000;
      
      
template <class T>
bool ProQueue<T>::isEmpty()
{
     return ( (Data.getSize()==0)?1:0 );
}


template <class T>
bool ProQueue<T>::isFull()
{
     return ( (Data.getSize()==Size)?1:0 );
}


template <class T>
void ProQueue<T>::enQueue(T el)
{
     if (Data.getSize() < Size)
	 {
		 Data.InsertAtLast(el);
	 }
}

template <class T>
void ProQueue<T>::enQueue(T el, unsigned int PR, bool AllowRepeat)
{
     if (Data.getSize() < Size)
	 {
		 T Temp;
		 Temp = this->First();
		 if ( (Data.getSize()==0) || (Temp._Priority > PR) )
		    Data.InsertAtFront(el);
         else
         {
             Temp = this->deQueue();
             enQueue(el, el._Priority , AllowRepeat);
             if (AllowRepeat)
                Data.InsertAtFront(Temp);
             else if (PR!=Temp._Priority)
                     Data.InsertAtFront(Temp);
         }
	 }
}

template <class T>
void ProQueue<T>::Clear()
{
     if (!isEmpty())
        Data.DeleteList();
}


template <class T>
T ProQueue<T>::deQueue()
{
	 T Temp;
     if (!isEmpty())
	 {
		 Temp = Data.RemoveAtFront();
	 }
     return (Temp);
}


template <class T>
T ProQueue<T>::First()
{
	 T Temp;
     if (!isEmpty())
	 {
		 Temp = Data.getHead()->getData();
	 }
     return ( Temp );
}