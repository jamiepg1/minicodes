#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
#include "Controller.h"
#include <fstream>


namespace DESCRIBER {

	/// <summary>
	/// Summary for Loader
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Loader : public System::Windows::Forms::Form
	{
	public:
		Loader(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}
		Loader(Controller* obj, char* str)
		{
			this->_Object = obj;
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			Config_All(str);
			//this->Close();
		}
		Loader(Controller* obj)
		{
			this->_Object = obj;
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			Config_All();
			//this->Close();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Loader()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  status;
	private: Controller* _Object;
	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Loader::typeid));
			this->status = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// status
			// 
			this->status->AutoSize = true;
			this->status->BackColor = System::Drawing::Color::Transparent;
			this->status->Font = (gcnew System::Drawing::Font(L"OCR A Extended", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->status->Location = System::Drawing::Point(42, 271);
			this->status->Name = L"status";
			this->status->Size = System::Drawing::Size(118, 17);
			this->status->TabIndex = 0;
			this->status->Text = L"LOADING....";
			// 
			// Loader
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackgroundImage = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"$this.BackgroundImage")));
			this->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Stretch;
			this->ClientSize = System::Drawing::Size(500, 300);
			this->Controls->Add(this->status);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->Name = L"Loader";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Loader";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion


	private: void Config_All(char* str)
			 {
				 //Temp-1
				 ifstream in;
				 in.open("C:/DESCRIBER/Temp-1.txt");
				 if (in)
				 {
					 //Load Files and set bool status sumwhere??
					 this->_Object->isLoad = true;
				 }
				 else
				 {
					 this->_Object->SetDocADDR(str);
					 this->_Object->isOpen = true;
				 }
				 in.close();
			 }

			 void Config_All()
			 {
				 //Temp-1
				 ifstream in;
				 in.open("C:/DESCRIBER/Temp-1.txt");
				 if (in)
				 {
					 //Load Files and set bool status sumwhere??
					 this->_Object->isLoad = true;
				 }
				 in.close();
			 }
	};
}
