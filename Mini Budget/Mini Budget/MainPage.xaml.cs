﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.IO.IsolatedStorage;
using System.IO;

namespace Mini_Budget
{
    public partial class MainPage : PhoneApplicationPage
    {

        static public string buget = "budget.txt";



        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
           //Application.
            base.OnBackKeyPress(e);
        }
        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Main_List.SelectedIndex == -1)
                return;
            
            if (Main_List.SelectedIndex == 0)
            {
                NavigationService.Navigate(new Uri("/New_Expense.xaml", UriKind.Relative));
            }
            else
                if (Main_List.SelectedIndex == 1)
                {
                    //Check Daily Expense
                    NavigationService.Navigate(new Uri("/Daily_Expense.xaml", UriKind.Relative));
                }
                
                    else
                        if (Main_List.SelectedIndex == 2)
                        {
                            //Change budget
                            NavigationService.Navigate(new Uri("/Change_Budget.xaml", UriKind.Relative));
             
                        }
                        else
                            if (Main_List.SelectedIndex == 3)
                            {
                                //Check Budget
                                try
                                {
                                    using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                                    {


                                        using (StreamReader stream = new StreamReader(store.OpenFile(buget, FileMode.Open, FileAccess.Read)))
                                        {
                                            MessageBox.Show("Your Current Budget is:"+stream.ReadLine(), "Information" , MessageBoxButton.OK);
                                            stream.Close();
                                        }
                                    }
                                }
                                
                                catch (Exception)
                                {
                                    MessageBox.Show("No Budget Defined");
                                }
                            }
                            else
                                if (Main_List.SelectedIndex == 4)
                                {
                                    NavigationService.Navigate(new Uri("/Add_Budget.xaml", UriKind.Relative));
                                }
             Main_List.SelectedIndex = -1;
        }
    }
}