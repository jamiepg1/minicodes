﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.IO.IsolatedStorage;
using System.IO;


namespace Mini_Budget
{
    public partial class New_Expense : PhoneApplicationPage
    {
        public New_Expense()
        {
            InitializeComponent();
            /* // Code to Write on FIle
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                DateTime dt = DateTime.Now;
                string filename = dt.Day.ToString()+" "+dt.Month.ToString()+" "+dt.Year.ToString();
                filename+=".txt";
                using (StreamWriter stream = new StreamWriter(store.OpenFile(filename , FileMode.OpenOrCreate , FileAccess.Write)))
                 {
                     stream.WriteLine("Hello World");
                 }
            }
            */
            
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (txt_Details.Text == string.Empty)
                {
                    MessageBox.Show("Enter Details", "Error", MessageBoxButton.OK);
                    return;
                }
                if (txt_Amount.Text == string.Empty)
                {
                    MessageBox.Show("Enter Amount", "Error", MessageBoxButton.OK);
                    return;
                }


                string bud;
                int amount = int.Parse(txt_Amount.Text);
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    string st = string.Empty;
                    DateTime dt = DateTime.Now;
                    string filename = dt.Day.ToString() + " " + dt.Month.ToString() + " " + dt.Year.ToString();
                    filename += ".txt";
                    
                    using (StreamReader stream = new StreamReader(store.OpenFile(MainPage.buget, FileMode.Open, FileAccess.Read)))
                    {

                        bud = stream.ReadLine();
                        stream.Close();
                    }
                    using (StreamReader stream = new StreamReader(store.OpenFile(filename, FileMode.OpenOrCreate, FileAccess.Read)))
                    {

                        while (!stream.EndOfStream)
                        {
                            st += stream.ReadLine() + "\n";
                        }
                        stream.Close();
                    }



                    using (StreamWriter stream = new StreamWriter(store.OpenFile(filename, FileMode.OpenOrCreate, FileAccess.Write)))
                    {
                        int budget = int.Parse(bud);
                        if (budget - amount < 0)
                        {
                            MessageBox.Show("Budget is less than  expenditure", "Information", MessageBoxButton.OK);
                            return;
                        }
                        using (StreamWriter stream2 = new StreamWriter(store.OpenFile(MainPage.buget, FileMode.Open, FileAccess.Write)))
                        {

                            stream2.WriteLine((budget - amount).ToString());
                            stream2.Close();
                        }
                        if (st != string.Empty)
                            stream.WriteLine(st.Trim());
                        stream.WriteLine(txt_Amount.Text);

                        stream.WriteLine(txt_Details.Text.Replace('\n', ' '));
                        stream.Close();
                    }
                }
                MessageBox.Show("Sucessfull");
                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            }
            catch (FormatException)
            {
                MessageBox.Show("Incorrect Amount", "Error", MessageBoxButton.OK);
                return;
            }
            catch (IsolatedStorageException)
            {
                MessageBox.Show("Please Update Budget", "Error", MessageBoxButton.OK);
                return;
            }

            
            /*
              //Code to Read from FIle
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    DateTime dt = DateTime.Now;
                    string filename = dt.Day.ToString() + " " + dt.Month.ToString() + " " + dt.Year.ToString();
                    filename += ".txt";
                    using (StreamReader stream = new StreamReader(store.OpenFile(filename, FileMode.OpenOrCreate, FileAccess.Read)))
                    {
                        
                        MessageBox.Show(stream.ReadLine() + " \n" + stream.ReadLine());
                        stream.Close();
                    }
                }*/
            
        }
    }
}