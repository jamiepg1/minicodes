﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace Mini_Budget
{
    public partial class Monthly_Budget : PhoneApplicationPage
    {
        public Monthly_Budget()
        {
            InitializeComponent();
        }

        private void btn_Submit_Click(object sender, RoutedEventArgs e)
        {
            if (txt_month.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Enter Month", "Error", MessageBoxButton.OK);
                return;
            }
            if (txt_year.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Enter Year", "Error", MessageBoxButton.OK);
                return ;
            }
            string error=" ";
            try
            {
                error = "Invalid Month" ;
                int month = int.Parse(txt_month.Text.Trim());
                error = "Invalid Year" ;
                int year = int.Parse(txt_year.Text.Trim());
                if (month > 12 || month <1)
                {
                    MessageBox.Show("Invalid Month", "Error", MessageBoxButton.OK);
                    return;
                }
                if (year < 0)
                {
                    MessageBox.Show("Invalid Year", "Error", MessageBoxButton.OK);
                    return;
                }
            }

            catch(FormatException)
            {
                MessageBox.Show(error , "Error" ,MessageBoxButton.OK);
                return;
            }
        }
    }
}