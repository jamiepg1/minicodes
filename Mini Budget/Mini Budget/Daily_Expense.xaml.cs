﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.IO.IsolatedStorage;
using System.IO;

namespace Mini_Budget
{
    public partial class Daily_Expense : PhoneApplicationPage
    {
        public Daily_Expense()
        {
            InitializeComponent();
        }

        private void btn_Submit_Click(object sender, RoutedEventArgs e)
        {
            if (txt_Date.Text == string.Empty)
            {
                MessageBox.Show("Enter Day", "Error", MessageBoxButton.OK);
                return;
            }
            if (txt_Month.Text == string.Empty)
            {
                MessageBox.Show("Enter Month", "Error", MessageBoxButton.OK);
                return;
            }
            if (txt_Year.Text == string.Empty)
            {
                MessageBox.Show("Enter Year", "Error", MessageBoxButton.OK);
                return;
            }
            string error = "Unknown Error";
            try
            {
                error = "Invalid Day" ;
                int day = int.Parse(txt_Date.Text.Trim());
                error = "Invalid Month" ;
                int month = int.Parse(txt_Month.Text.Trim());
                if (month > 12 || month <= 0)
                {
                    MessageBox.Show(error, "Error", MessageBoxButton.OK);
                    return;
                }
                if (day > 31)
                {
                    MessageBox.Show("Invalid date", "error", MessageBoxButton.OK);
                    return;
                }
                error = "Invalid Year" ;
                int year = int.Parse(txt_Year.Text.Trim());
                if (month == 2)
                {
                    if (day == 29)
                    {
                        if (year % 4 != 0)
                        {
                            MessageBox.Show(error, "error", MessageBoxButton.OK);
                            return;
                        }
                    }
                    if (day > 29)
                    {
                        MessageBox.Show("Invalid date ", "Error", MessageBoxButton.OK);
                        return;
                    }
                }
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    string filename = txt_Date.Text.Trim()+ " " + txt_Month.Text.Trim()+ " " + txt_Year.Text.Trim();
                    filename += ".txt";
                    using (StreamReader stream = new StreamReader(store.OpenFile(filename, FileMode.Open, FileAccess.Read)))
                    {
                        while (!stream.EndOfStream)
                            Data.Items.Add("Amount = " + stream.ReadLine() + "\nDetail:" + stream.ReadLine()); 
                    }
                }
            }
            catch(FormatException)
            {
                MessageBox.Show(error , "Error" , MessageBoxButton.OK);
                return;
            }
            catch(IsolatedStorageException)
            {
                MessageBox.Show("No Budget of this Date Found" , "Information" , MessageBoxButton.OK);
                return;
            }
        }
    }
}