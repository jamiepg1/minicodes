#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <unistd.h> 
#include <arpa/inet.h>
#include <fstream>
#include <dirent.h>
#include <iostream>

using namespace std;

#define SERVER_PORT 6000




struct payload
{
	char message[255];
	int sequence_number;
	bool ack;
};

struct payload* new_payload()
{
    struct payload *new_payload;
    new_payload = new payload;
    new_payload->sequence_number = 0;
    new_payload->ack = 0;
    memset(new_payload->message, '\0', 255);
    return new_payload;
}

int main(void)
{
	// Creating socket
	int sock = socket(AF_INET, SOCK_DGRAM, 0);
	if(sock ==-1)
	{
		printf("Error Creating Socket");
		exit(EXIT_FAILURE);
	}
	
	struct sockaddr_in sa;
	
	int length = sizeof(sa);
    	memset(&sa, 0, sizeof sa);
    
        sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = inet_addr("127.0.0.1");  
	sa.sin_port = htons(SERVER_PORT);

	// Binding socket
	
	struct sockaddr_in from;
	memset(&from, 0, sizeof(from));
	int fromlen = sizeof(struct sockaddr_in);;
	

    

     for (int i = 0 ; i < 10 ; i++)
     {
         bool b = 1;
         while (b)
	 {
	   // Making file list packet
	   struct payload *file_list_payload;
	   file_list_payload = new_payload();
	
	   file_list_payload->sequence_number = i;
	   strcpy(file_list_payload->message, "message");
	
	   // Sending file lise packet
	   sendto(sock, (void *)file_list_payload, sizeof(struct payload), 0, (struct sockaddr *)&sa, sizeof(sockaddr_in));
	
	   struct payload *recv_payload;
	   recv_payload = new_payload();
	
	   int recsize;
	
	   // Waiting for connection
	   recsize = recvfrom(sock, recv_payload, sizeof(struct payload), 0, (struct sockaddr *)&from, (socklen_t*)&fromlen);
	   if(recsize < 0)
		perror("recvfrom");
	
	   if (recv_payload->ack == 1)
	   {
		cout<<"Packet Number "<<recv_payload->sequence_number << " Acknowledged"<<endl;
	   	b = 0;
	   }
	   else
	   {
		cout<<"Retransmitting Packet Number "<<i <<endl;
		//cout <<"Retransmitting "<<endl; 
	   }
	}
     }
    close(sock);
    return 0;
}



