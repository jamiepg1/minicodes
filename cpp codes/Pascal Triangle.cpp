#include <iostream>


using namespace std;

int Number(int r, int p)
{
	if (p == 1 ||  ( p == r)) return 1;
	else return (Number(r-1, p-1) + Number(r-1, p));	
}

void PTriangle(int n)
{
	for (int i = 1 ; i <= n; i++)
	{
		int times = n-i;
		for (int j = 1; j <= times; j++)
		{
			cout << " ";
		}
		for (int j = 1; j <= i; j++)
		{
			cout << Number(i, j) << " ";
		}
		cout << endl;
	}
}

int main()
{
	int input;

	cout << "Input Number > ";
	cin >> input;
	
	PTriangle(input);

	system("pause");
	return 0;
}
