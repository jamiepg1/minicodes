#include <iostream>
using namespace std;
void print(int data[] , int count = 10)
{
	cout << "<----- Printing Array  ----->" << endl;
	for (int i = 0; i < count; i++)
		cout << data[i] << endl;
}
// NOTE ARRAY IS SORTED ON 30 TO 0 BIT BUT ITS NOT SORTING ON UR GIVEN BITS

bool GetBit(int key, int bitPosition)
{	
	return ((key >> bitPosition) & 1);
}
void RadixSort(int data[], int lower, int upper, int bit = 30)
{
	if (bit > 19 && lower<upper)
	{//
		int i = lower;
		int j = upper;
	
		while (j >= i)
		{
            while (!GetBit(data[i], bit))i++;
			
			while (GetBit(data[j], bit))j--;
			
			if (j > i)
				swap(data[i++], data[j--]);
		}
		
			RadixSort(data, lower, j, bit-1);
			RadixSort(data, j+1, upper, bit-1);
	}
}

int main()
{
	system("cls");
	
const int count = 10;
	int data[count]={34562,12,234,123,3456,7684,12003,12903,506,2008};
	
    RadixSort(data, 0, count);
	print(data); 
    cout << endl;
	system("pause");
	return 0;
}



