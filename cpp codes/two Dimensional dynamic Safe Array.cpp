#include <iostream>

using namespace std;

class DS2DA{
      private:
              int **data;
              unsigned int column;
              unsigned int row;
      public:
              DS2DA():data(0),column(0),row(0){}
              
              DS2DA(unsigned int c, unsigned int r)
              {
                   column=c;
                   row=r;
                   
                   data=new int*[c];
                   
                   for (int i=0; i<c; i++)
                   {
                       *data=new int[r];
                       
                       for (int j=0; j<r; j++)
                           **(data+j)=0;
                   }
              }
              
              DS2DA(DS2DA &rhs)
              {
                   if ((column!=rhs.get_column())||(row!=get_row()))
                   {
                       for (int i=0; i<column; i++)
                                    delete[] *(data+i);
                                    
                       delete[] data;
                       
                       column=rhs.get_column();
                       row=rhs.get_row();
                       
                       data=new int*[column];
                       
                       for (int i=0; i<column; i++)
                            *data=new int[row];                               
                   }
                   
                   for (int i=0; i<column; i++)
                       for (int j=0; j<row; j++)
                           *(*(data+i)+j)=rhs(i,j);              
                   
              }
              
              unsigned int get_column()
              {
                       return (column);
              }
              
              unsigned int get_row()
              {
                       return (row);
              }
              
              DS2DA operator= (DS2DA &rhs)
              {
                   if ((column!=rhs.get_column())||(row!=rhs.get_row()))
                   {
                       for (int i=0; i<column; i++)
                           delete[] *(data+i);
                           
                       delete[] data;
                       
                       column=rhs.get_column();
                       row=rhs.get_row();
                       
                       data=new int*[column];
                       
                       for (int i=0; i<column; i++)
                           *data=new int[row];
                   }
                   
                   for (int i=0; i<column; i++)
                       for (int j=0; j<row; j++)
                           *(*(data+i)+j)=rhs(i,j);
              }
              
              int& operator() (unsigned int c, unsigned int r)
              {
                   if ((c<column)&&(r<row))
                   {
                       return *(*(data+c)+r);
                   }
                   else cout<<"Error";
              }
              
              const int& operator() (unsigned int c, unsigned int r)const
              {
                   if ((c<column)&&(r<row))
                   {
                       return *(*(data+c)+r);
                   }
                   else cout<<"Error";
              }
              
              ~DS2DA()
              {
                      if (data)
                      {
                               for (int i=0; i<column; i++)
                                    delete[] *(data+i);
                                    
                               delete[] data;
                               
                               column=0;
                               row=0;
                      }
              }
};


/*
int main()
{
    
    
    system("pause");
    return(0);
}
*/
