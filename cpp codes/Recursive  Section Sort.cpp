#include<iostream>
using namespace std;



void selectionSort(int *array, int s,int l)
{
    
    if ( s >=l - 1)
        return;
    int min = s;
    for ( int i = s + 1; i < l; i++ )
    {
        if (array[i] < array[min] )
            min = i;
    }
    int temp = array[s];
    array[s] = array[min];
    array[min] = temp;
    selectionSort(array, s + 1 , l);
}



int main()
{
    const int j = 11;
    int a[j]={3,19,4,10,11,23,1,9,2,6,7};
   
    selectionSort(a,0,j);
    
    for ( int i = 0 ; i < j ; i++)
          cout<<a[i]<<endl;
    
    cout<<endl<<endl<<endl;
    system("pause");
    return(0);
}
