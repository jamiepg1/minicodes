#include <iostream>
#include <fstream>
#include <conio.h>

using namespace std;

#define count 25000

int GetBit(int key, int bitPosition);
void RadixSort(int data[], int left, int right, int bit = 31);
void Swap(int& a, int& b);
void ReadData(int data[]);
void PrintData(int data[]);
bool IsSorted(int data[]);

int main()
{
	system("cls");
	
	int data[count];
	
    ReadData(data);
	RadixSort(data, 0, count);
	if (IsSorted(data))
		cout << "Data Sorted Successfully!" << endl; 
	else
		cout << "Data NOT Sorted Successfully!" << endl; 
	
	cout << endl;
	system("pause");
	return 0;
}

int GetBit(int key, int bitPosition)
{	
	return ((key >> bitPosition) & 1);
}

void RadixSort(int data[], int left, int right, int bit)
{
	if (bit >= 0)
	{
		int i = left;
		int j = right;
	
		while (i <= j)
		{
			while (!GetBit(data[i], bit))
				i++;
			
			while (GetBit(data[j], bit))
				j--;
			
			if (i < j)
				Swap(data[i++], data[j--]);
		}
		
		if (left < j)
			RadixSort(data, left, j, bit-1);
		if (j+1 < right)
			RadixSort(data, j+1, right, bit-1);
	}
}

void Swap(int& a, int& b)
{
	int temp;
	temp = a;
	a = b;
	b = temp;
}

void ReadData(int data[])
{
	ifstream in;
	in.open("Input.txt");
	
	int i = -1;
	while (!in.eof())
	{
		in >> data[++i];
	}
	
	in.close();
}

void PrintData(int data[])
{
	cout << "----- Printing Array Data -----" << endl;
	for (int i = 0; i < count; i++)
		cout << data[i] << endl;
}

bool IsSorted(int data[])
{
 	for (int i = 1; i < count; i++)
    {
        if (data[i-1] <= data[i]) 
			continue;
        else
		{
			cout << data[i-1] << " " << data[i] << " Position = " << i << endl;
			return false;
		}
    }
    return true;  
}
