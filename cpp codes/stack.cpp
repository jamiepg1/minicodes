#include<iostream>
using namespace std;
template <class T>
class snode{
        public: 
                T data;
                snode<T> *next;
                snode()
                {
                       data = 0;
                       next = NULL;
                }
                snode(T  d)
                {
                         data = d;
                         next = NULL;
                }
};
template <class T>
class stack
{
      private:
              snode<T> *top;
              void deleteall()
              {
                   if (top)
                   {
                           snode<T> *t ;
                           t = top;
                           
                           while(top)
                           {
                                   t = top;
                                   top=top->next;
                                   t->next = NULL;
                                   delete t;
                           }
                   top = NULL;
                   }
              }
      public:
             stack()
             {
                     top = NULL;
             };
             ~stack()
             {
                     if (top)
                        deleteall();
             }
             bool isempty()
             {
                  if (top)
                     return false;
                  else
                      return true;
             }
             void push(T d)
             {
                  if (!top)
                  {
                           top = new snode<T>;
                           top->next = NULL;
                           top->data = d;
                  }
                  else
                  {
                           snode<T> *temp = new snode<T>;
                           temp->data = d;
                           temp->next = top;
                           top = temp;
                  }
             }
             T pop()
             {
                   T d;
                   d = top -> data;
                   snode<T> *temp;
                   temp = top;
                   top = top -> next;
                   temp->next = NULL;
                   delete temp;
                   return d;
             }
             void print()
             {
                 if (top)
                 {
                         snode<T> *c;
                         c = top;
                         while (c)
                         {
                               cout<<c->data << "   ";
                               c=c->next ;
                         }
                 }
                 cout<<endl;
             }
             T peek()
             {
                 if(top)
                 {
                  return top->data;
                 }
                 else
                     return 0; 
             }
             void flush()
             {
                  if (top)
                     deleteall();
             }
};
/*
int main()
{
    stack <char*> obj;
    obj.push("hello");
    
    obj.push("hii");
    obj.push("hii2");
    obj.print();
    cout<<obj.pop()<<endl;
    cout<<obj.peek()<<endl;
    system("pause");
    return 0;
    
}*/
