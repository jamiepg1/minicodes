#include<iostream>
using namespace std;
class btnode{
      int data ;
      btnode *right , *left;
      public:
      btnode()
      {
              data = 0;
              right = NULL;
              left = NULL;
      }
      btnode(int d)
      {        
              data = d;
              right = NULL;
              left = NULL;
      
      }
friend class bst;
};
class bst{
          private :
          btnode *top ;  
           void deltree(btnode *c)
            {
               if (c)  // cehecking if root if data is available 
               {
                     deltree(c->left); // go to its left node
                     deltree(c->right);// go to its right node
                     delete c;  //currennt node; 
                     c = NULL;  
               }
               top = NULL;
          }
          bool subtree ( btnode *current , btnode *current2)
          {
               if (!current2 ->left && !current2 ->right && current2->data == current ->data)
               {
                                return true;
               }
               if (!current2 ->left && !current2 ->right && current2->data != current ->data)
               {
                   return false;
               }
               if (current2 ->left && !current->left)
                  return false;
               if (current2->right && !current->right)
               return false;
              if ( current2 ->left)
              { 
                   if (current2 ->left->data  != current->left->data)
                      {
                                return false;
                      }
                      
                   return subtree (current->left , current2->left);
              }
              if (current2->right)
              {             
                  if (current2 ->right->data  != current->right->data)
                  {
                      
                      return false;
                  }
                  return  subtree (current->right , current2->right);
              }   
          }

          void insert (int d , btnode *c)
          {
              
              if (d <= c->data && c->left == NULL)
              { 
                    btnode *temp ;
                    temp = new btnode;
                    temp->data = d; 
                    c->left = temp; 
              }
              else
              if (d > c->data && c->right == NULL)
              {
                     btnode *temp ;
                      temp = new btnode;
                    temp->data = d;
                    c->right = temp;
              }
              else
              if (d > c->data)
              {
                    insert(d , c->right);
              }
              else
                  insert (d , c -> left);
          }
          
          void Preorder(btnode *t)   
          {
		     if (t != 0)
		     {
              cout << t->data << endl; 
              Preorder(t->left); 
		      Preorder(t->right);
		     }
          }
          
          public :
          bst()
          {
               top = 0;
          }
          bst (int d)
          {
              top = new btnode;
              top->data = d;
          }
          ~bst()
          {
                if (top)
                deltree(top);
          }
          void addchild(int d )
          {
               if (!top)
               {
                        top = new btnode;
                        top->data = d; 
               }
               else
               insert(d , top); 
          }
          void print ()
          {
               if (top)
               {
               Preorder(top);
             
               }
               
          }
          bool sub ( bst & rhs)
          {
               btnode *current;
               current = top;
               
               while (current != NULL && current->data != rhs.top->data)
               {
                     if ( current -> data < rhs.top->data)
                          current = current->left;
                     if (current ->data >rhs.top->data)
                          current= current->right;
               }
               if (current)
               {
                 if (!rhs.top->right && !rhs.top->left)
                    return true;
                  return subtree(current , rhs.top);
               }
               else
               {
                return 0;
               }
          }
      
      };
int main()
{
    bst trr;
    trr.addchild(15);
    trr.addchild(6);
    trr.addchild(45);
    trr.addchild(4);
    trr.addchild(5);
    trr.addchild(7);
    bst t ;
    t.addchild(15);
    t.addchild(6);
    t.addchild(45);
    t.addchild(4);
    t.addchild(7);
    trr.print();
    bool sub=trr.sub(t);
   if (sub)
      cout <<"it is a sub tree "<<endl;
   else
       cout<<"it is not a sub tree"<<endl;
    system("pause");
    return 0;
}
