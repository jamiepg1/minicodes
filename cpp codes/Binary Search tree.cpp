#include <iostream>

using namespace std;


template <class T>
class TreeNode{
      
      private:
              T Data;
              TreeNode<T> *Left;
              TreeNode<T> *Right;
      public:
             TreeNode(T d){Data = d; Right = NULL; Left = NULL;}
             ~TreeNode(){Data = 0; Right = NULL; Left = NULL;}
             T getData(){ return Data;}
             TreeNode<T>* getLeft(){return Left;}
             TreeNode<T>* getRight(){return Right;}
};
      
template <class T>
class BinaryTree{
      
      private:
              TreeNode<T> *Root;
              void Tree_Insert(TreeNode<T> *root, T data);
      public:
             BinaryTree(){Root = NULL;}
             ~BinaryTree(){Root = NULL;}
             void Insert(T NewData){Tree_Insert(Root,NewData);}
             void InorderPrint(TreeNode<T> *root);
      
      };
      
      
template <class T>
void BinaryTree<T>::InorderPrint(TreeNode<T> *root)
{
     if (Root!=NULL)
     {
                    InorderPrint(root->Left);
                    cout<<root->getData()<<"  ";
                    InorderPrint(root->right);
     }
}


template <class T>
void BinaryTree<T>::Tree_Insert(TreeNode<T> *root, T data)
{
     if (Root==NULL)
     {
        Root = new TreeNode<T> (data);
        return;
     }
     else if (data < root->getData())
             Tree_Insert(root->getLeft(),data);
     else if (data > root->getData())
             Tree_Insert(root->getRight(),data);
}
      
      
int main()
{
    BinaryTree<int> obj;
    for(int i=0; i<10; i++)
    obj.Insert(i);
    system("pause");
    return 0;
}
