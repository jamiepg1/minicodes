#include <iostream>

using namespace std;

class DSJA{
      private:
              int **data;
              unsigned int *columns;
              unsigned int row;
              
      public:
             DSJA():data(0),columns(0),row(0){}
             
             DSJA(unsigned int rows,int *col)
             {

                  rows=rows;
                  
                  columns=new unsigned int[rows];
                  
                  for (int i=0; i<rows; i++)
                      *(columns+i)=*(col+i);
                  
                  data=new int*[rows];
                  

                  for (int i=0; i<rows; i++)
                  {
                       *(data+i)=new int[columns[i]];
                       
                       for (int j=0; j<columns[i]; j++)
                           *(*(data+i)+j)=0;
                  }
                  
             }
             
             DSJA (DSJA &rhs)
             {
                  row = rhs.get_row();                      
                  columns=new unsigned int[row];
                  
                  for (int i=0; i<row; i++)
                      *(columns+i)=*(rhs.get_column()+i);    
                          
                  data=new int*[row];
                      
                  for (int i=0; i<row; i++)
                      *data=new int[columns[i]];              
                  
                  for (int i=0; i<row; i++)
                      for (int j=0; j<columns[i]; j++)
                          *(*(data+i)+j)=rhs(i,j);
             }
             
             unsigned int get_row()
             {
                 return (row);
             }
             
             unsigned int* get_column()
             {
                  return columns;
             }
             
             
             DSJA operator = (DSJA &rhs)
             {
                  bool flag=true;
                  
                  for (int i=0; i<row; i++)
                  {
                      if ((i>=rhs.get_row())&&(columns[i]!=rhs.get_column()[i]))
                      {
                         flag=false; break;
                      }
                  }
                  
                  if ((row!=rhs.get_row())||(flag=false))
                  {
                      for (int i=0; i<row; i++)
                          delete[] *data;
                          
                      delete[] data;
                      
                      row = rhs.get_row();                      
                      columns=new unsigned int[row];
                  
                      for (int i=0; i<row; i++)
                          *(columns+i)=*(rhs.get_column()+i);    
                          
                      data=new int*[row];
                      
                      for (int i=0; i<row; i++)
                          *data=new int[columns[i]];              
                  }
                  
                  for (int i=0; i<row; i++)
                      for (int j=0; j<columns[i]; j++)
                      {
                          *(*(data+i)+j)=rhs(i,j);
                      }
                  
             }             
             
             int& operator () (unsigned int r, unsigned int c)
             {
                  if ((r<row)&&(c<columns[r]))
                  {
                      return *(*(data+r)+c);
                  }
                  else cout<<"Error";
             }
             
             const int& operator () (unsigned int r, unsigned int c)const
             {
                  if ((r<row)&&(c<columns[r]))
                  {
                      return *(*(data+r)+c);
                  }
                  else cout<<"Error";
             }
             
             ~DSJA()
             {
                    if (data)
                    {
                             for (int i=0; i<row; i++)
                                 delete[] *data;
                             
                             delete[] data;
                             
                             row=0;
                             delete[] columns;
                    }
             }
};

int main()
{
    
    
    system("pause");
    return (0);
}
