/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman;

import java.awt.Image;
import java.util.ArrayList;

/**
 *
 * @author Malik Ali
 */
public class Boss extends Enemy{
    private ArrayList<Image>enemyImage = new ArrayList<>();
    private int addChild = 1000;
    /*
     * 1 up
     * -1 down
     * 2 left
     * -2 right
     */
    
    public Boss(String path , int count ,int x , int y , int p)
    {
 
        super(path, count, x, y , p);
        super.setEmpty("sprites/blankbig.png");
    }
    @Override
    public int getHeight()
    {
        return height;
    }
    
    @Override
    public int getWidth()
    {
        return width;
    }
    
    @Override
    public int getBufferValue()
    {
        return BufferValue;
    }
    
    
    
    @Override
    public boolean killMe()
    {
        return super.killMe();
        
    }
    
    @Override
    public Image getImage()
    {
        addChild--;
        if(addChild == 0)
        {
            addChild = 1000;
            MainClass.addEnemy(super.positionx, super.positiony);
        }
        return super.getImage();
    }
    
}
