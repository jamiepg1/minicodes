/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman;

import java.awt.Image;
import java.util.ArrayList;

/**
 *
 * @author Malik Ali
 */
public class Enemy {
    protected ArrayList<Image>enemyImage = new ArrayList<>();
    private Image empty = MainClass.loadImage("sprites/blank.png");
    protected int power;
    public  int positionx;
    public  int positiony;
    protected int height;
    protected int width;
    protected int Imune = 0;
    protected int Index = 0;
    protected int BufferValue = 5;
    public boolean isKilled = false;
    protected int change = 0;
    protected int steps = 100;
    /*
     * 1 up
     * -1 down
     * 2 left
     * -2 right
     */
    protected int movement;
    public Enemy(String path , int count ,int x , int y )
    {
        for(int i = 1 ; i <= count ; i++)
            enemyImage.add(MainClass.loadImage(path+i+".png"));
        height = enemyImage.get(0).getHeight(null);
        width = enemyImage.get(0).getWidth(null);
        positionx = x;
        positiony = y;
        int r = MainClass.stage[x-1][y];
        int r2 = MainClass.stage[x-1][y+height];
        int up1 = MainClass.stage[x][y-1];
        int up2 = MainClass.stage[x+width][y-1];
        if(r != -2 && r2!=-2 && up1!=-2 && up2!=-2)
        {
            movement =   ((int)(100* Math.random())%4);
            if(movement == 0)
                movement =-1;
            else
            if (movement == 3)
                movement = -2;
        }
        power = 0 ;
        movement = 1;
    }
    protected void setEmpty(String path)
    {
        empty = MainClass.loadImage(path);
    }
    
    
    public Enemy(String path , int count ,int x , int y , int p)
    {
 
        power = p;
        for(int i = 1 ; i <= count ; i++)
            enemyImage.add(MainClass.loadImage(path+i+".png"));
        height = enemyImage.get(0).getHeight(null);
        width = enemyImage.get(0).getWidth(null);
        positionx = x;
        positiony = y;
        int r = MainClass.stage[x-1][y];
        int r2 = MainClass.stage[x-1][y+height];
        int up1 = MainClass.stage[x][y-1];
        int up2 = MainClass.stage[x+width][y-1];
        if(r != -2 && r2!=-2 && up1!=-2 && up2!=-2)
        {
            movement =   ((int)(100* Math.random())%4);
            if(movement == 0)
                movement =-1;
            else
            if (movement == 3)
                movement = -2;
        }
        movement = 1;
    }
    public int getHeight()
    {
        return height;
    }
    
    public int getWidth()
    {
        return width;
    }
    
    public int getBufferValue()
    {
        return BufferValue;
    }
    
    
    protected void calculateMovement()
    {
        if(movement == -1)
        {
            int temp = MainClass.stage[positionx+getWidth()][positiony+getHeight()+1];
            int temp2 = MainClass.stage [positionx ] [ positiony + getHeight()+1 ];
            if( temp == 2 ||  temp == -2 || temp2 == 2 || temp2 == -2)
            {
                int n = (int ) (100 * Math.random()) ; 
                n %= 2 ;
                
              if ( n == 1 )
                movement = 2;
              else 
                  movement = -2 ;
              
            }
            else 
                if( temp == 3 ||  temp == -3 || temp2 == 3 || temp2 == -3)
            {
                movement = -movement;
            }
        }
        if(movement == 1)
        {
            int temp = MainClass.stage[positionx+getWidth()][positiony - 1];
            int temp2 = MainClass.stage [positionx ] [ positiony  - 1 ];
            if( temp == 2 ||  temp == -2 || temp2 == 2 || temp2 == -2)
            {
                    int n = (int ) (100 * Math.random()) ; 
                n %= 2 ;
                
              if ( n == 1 )
                movement = 2;
              else 
                  movement = -2 ;
             
                
            }
            else 
                if( temp == 3 ||  temp == -3 || temp2 == 3 || temp2 == -3)
            {
                movement = -movement;
            }
        }
        if(movement == 2)
        {
            int temp = MainClass.stage[positionx - 1][positiony];
            int temp2 = MainClass.stage [positionx - 1] [ positiony + getHeight() ];
            if( temp == 2 ||  temp == -2 || temp2 == 2 || temp2 == -2)
            {
                int n = (int ) (100 * Math.random()) ; 
                n %= 2 ;
                
              if ( n == 1 )
                movement = 1;
              else 
                  movement = -1 ;
              
            }
            else 
                if( temp == 3 ||  temp == -3 || temp2 == 3 || temp2 == -3)
            {
                movement = -movement;
            }
        }
        if(movement == -2)
        {
            int temp = MainClass.stage[positionx+getWidth() + 1][positiony+getHeight()];
            int temp2 = MainClass.stage [positionx+getWidth() + 1 ] [ positiony  ];
            if( temp == 2 ||  temp == -2 || temp2 == 2 || temp2 == -2)
            {
                int n = (int ) (100 * Math.random()) ; 
                n %= 2 ;
                
              if ( n == 1 )
                movement = 1;
              else 
                  movement = -1 ;
              
            }else 
                if( temp == 3 ||  temp == -3 || temp2 == 3 || temp2 == -3)
            {
                movement = -movement;
            }
        }
        steps--;
        if(steps == 0)
        {
          
            
            steps = ( (int)(9999 *Math.random())%60 );
            change =   ((int)(100* Math.random())%4);
            
            if(change == 0)
                change =-1;
            else
            if (change == 3)
                change = -2;
        }
        if(change!=0)
        {
            if(change == -movement)
                movement = -change;
            else 
            if(change == 2|| change == -2)
            {
                int r = MainClass.stage[positionx-1][positiony];
                int r2 = MainClass.stage[positionx-1][positiony+width];
                if(r==0 &&r2==0)
                    movement = change;
            }
            else
            {
                int r = MainClass.stage[positionx][positiony-1];
                int r2 = MainClass.stage[positionx+width][positiony-1];
                if(r==0 &&r2==0)
                    movement = change;
            }
            change = 0;
        }
        if(movement == 1)
        {
            if(positiony == 1)
                movement = -1;
            else
                positiony--;
        }
        else
        if(movement == -1)
        {
            if(positiony+height == MainClass.resy-1)
                movement = 1;
            else
                positiony++;
        }else
        if(movement == 2)
        {
            if(positionx == 1)
                movement = -2;
            else
               positionx--;
        }
        else
        if(movement == -2)
        {
            if(positionx+width == MainClass.resx+1)
                movement = 2;
            else
              positionx++;
        }
        
        /*
        if(up)
        {
            if(positiony==1)
                up = false;
            positiony--;
        }
        else
        {
            if(positiony+getHeight() == MainClass.resy-1)
                up = true;
            positiony++;  
            
        }*/
    }
    public boolean killMe()
    {
        if(power == 0)
            return true;
        else
        {
            if(Imune ==0)
            {
                power--;
                Imune = 100;
            }
        }
        return false;
    }
    public Image getImage()
    {
        calculateMovement();
        Index++;
        if(Imune !=0)
        {
            Imune--;
            if( Imune%2 == 1)
            return empty;
        }
            
        if(Index == enemyImage.size()+2)
            Index=0;
        return enemyImage.get(Index%2);
    }
    public Image kill()
    {
        isKilled = true;
        return null;
    }
}
