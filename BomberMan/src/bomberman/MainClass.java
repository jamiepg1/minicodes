/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman;

import java.awt.DisplayMode;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author Malik Ali
 */
public class MainClass implements KeyListener , MouseListener{

    boolean running = true;
    private int menuIndex = 1;
    private ScreenManager screen;
    private Image bgImage;
    private Image brickImage = MainClass.loadImage("sprites/brick.png");
    private ArrayList<Brick> Bricks ;
    private static ArrayList<Enemy> Enemies ;
    private int up = -1;
    private int down = 0;
    private int left = -1;
    private int right = -1;
    int resx1 = 640;
    int resy1 = 480;
    public static int resx = 640;
    public static int resy = 480;
    public boolean killed = false;
    private int movementFactor = 1;
    private int imagesUsed = 8;
    private int bombsAllowed = 2;
    public static int [][] stage;
    private Boss boss ;
    public static boolean changeStage = false;
    private static int bomberManLives = 3;
    private BomberMan hero = new BomberMan(bomberManLives, 0, 0);
    private ArrayList<Bomb> bombes = new ArrayList<>();    
    private SoundPlayerMidi soundPlayer;
    
    private Image imagePause =  loadImage("sprites/pause.png");
    //Working For Multiple Stage
    private ArrayList<Image> stageStarterImages = new ArrayList<>();
    private int startUpCounter = 20;
    private boolean isStarted = false;
    private ArrayList<Image> BackGroundImages = new ArrayList<>();
    private int stageNumber = 0;
    private ArrayList<String> stageSounds = new ArrayList<>(); 
    private int totalStages = 3;
    private boolean finished = false;
    private boolean isPaused = false;
    private boolean killedBoss = false;
    
    private boolean inStartMenu = true;
    
    public static void main(String[] args) 
    {
    
        MainClass Game = new MainClass();
        Game.run();
        
        Game.startMenu();
        Game.StoryLoop();
        
        while(true)
        {
            Game.PreProcessing();
            
            
            Game.animationLoop();
            
            Game.Stop();
            
            if( Game.hero.getLives()<=0)
            {
                Game.screen.restoreScreen();
                Game.killed = false;
                break;
            }
            if(changeStage)
            {
                
                changeStage = false;
                Game.newStage();
                
            }
            if(Game.finished)
            {
                Game.EndGame();
                Game.screen.restoreScreen();
                //Acknowledge 
                System.exit(0);
            }
            Game.killed = false;
        }
    }
  
    
    private void startMenu()
    {
        ArrayList<Image> img = new ArrayList<>();
        for(int i = 1 ; i <=4 ; i++)
        {
            img.add(loadImage("sprites/startScreen/"+i+".png"));
        }
        for(int i = 0 ; i  < 100 ; i++)
        {
            Graphics2D g = screen.getGraphics();
            
            
            //Do Everything in this Function
            g.drawImage(img.get(0) , up, up, null);
    
            
            g.dispose();
            
            screen.update();
            
            // take a nap
            try {
                Thread.sleep(30);
            }
            
            catch (InterruptedException ex) { }
        }
        
        while (running) {
       
             
            // draw and update screen
            if(menuIndex == -1)
                return;
            Graphics2D g = screen.getGraphics();
            
            
            //Do Everything in this Function
            
            g.drawImage(img.get(menuIndex) , up, up, null);
    
            
            g.dispose();
            
            screen.update();
            
            // take a nap
            try {
                Thread.sleep(30);
            }
            catch (InterruptedException ex) { }
        }
        
        
        
    }
    private void newStage()
    {
        System.out.println(stageNumber);
        if (stageNumber+1 == totalStages)
        {
            System.out.println("Stages"+stageNumber);
            finished = true;
        }
        else
            stageNumber++;
    }
    public void Stop()
    {
        soundPlayer.stop();
    }
   
    private void loadSound()
    {
        for(int i = 1 ; i <= totalStages ; i++)
        {
            stageSounds.add("sounds/"+i+".mid");
        }
    }
    private void loadImages() {
        // load images
        for(int i = 1 ; i<=totalStages ; i++)
        {
            BackGroundImages.add(loadImage("sprites/background/"+i+".png"));
            stageStarterImages.add(loadImage("sprites/stage/"+i+".png"));
        }
        
    }
    
    
    private void loadResources()
    {
        
        loadImages();
        loadSound();
    }
    
    
    public MainClass() 
    {
        
        loadResources();
    }
    
    private void Initialize()
    {
        if(bombes!= null)
            bombes.clear();
        if(Bricks != null)
            Bricks.clear();
        if(Enemies != null)
            Enemies.clear();
        Bricks = new ArrayList<>();
        Enemies = new ArrayList<>();
        hero.positionx = 0;
        hero.positiony = 0;
        bgImage  = BackGroundImages.get(stageNumber);
        startUpCounter = 50;
        isStarted = false;
        //bgImage =loadImage("sprites/background/b3.jpg"); 
    }
    
    private void PreProcessing()
    {
        Initialize();
       FileInputStream fstream = null;
        try {
            soundPlayer = new SoundPlayerMidi(stageSounds.get(stageNumber));
            soundPlayer.playSound();
            stage = new int[resx1][resy1];
            
            fstream = new FileInputStream("stages/"+(stageNumber+1)+".in");
            // Get the object of DataInputStream
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            //Read File Line By Line
            
            for(int i = 0 ; i < resx1 ; i++)
            {
                strLine = br.readLine();
                String []s = strLine.split(" ");
                for(int j = 0 ; j < resy1 ; j++)
                {
                     int x =  Integer.parseInt(s[j]);
                     if (x == 2)
                     {
                         Brick bri = new Brick(i , j);
                         Bricks.add(bri);
                     }
                     
                     stage[i][j] = x;
                }
            }
            int h =0;
            resx = 620;
            resy= 428;
            for(int i = 0 ; i < resx ; i++)
            {
                for(int j = 0 ; j < resy ; j++)
                {
                    if (stage[i][j] == 5)
                    {
                        h++;
                        if(h%2 == 0)
                        {
                            Enemy e = new Enemy("sprites/enemy/enemy1/", 2 , i , j );
                            Enemies.add(e);
                        }
                        else
                        {
                            Enemy e = new Enemy("sprites/enemy/enemy2/", 2 , i , j);
                            Enemies.add(e);
                        }
                    }
                }
            }
            boss = new Boss("sprites/enemy/enemybig/", 2, 200, 100, 10);
        
        } catch (IOException ex) {
            Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally {
            try {
                fstream.close();
            } catch (IOException ex) {
                Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
    }
    
    public static void addEnemy(int x , int y)
    {
                    Enemy e = new Enemy("sprites/enemy/enemy3/", 2 , x , y , 2);
                    Enemies.add(e);
                    
    }
    
    private static final DisplayMode POSSIBLE_MODES[] = 
    {
        new DisplayMode(640, 480, 32, 0),
     
    };
    
    
    
    
    
    public static Image loadImage(String fileName) {
        return new ImageIcon(fileName).getImage();
    }
    
    
    public void run() {
        screen = new ScreenManager();
        
        
        try {
            DisplayMode displayMode =
                screen.findFirstCompatibleMode(POSSIBLE_MODES);
            screen.setFullScreen(displayMode);
        
            
            Window win =   screen.getFullScreenWindow();
            win.setFocusTraversalKeysEnabled(false);
            win.addKeyListener(this);
            win.addMouseListener(this);
          
            
        }
        finally {
            // screen.restoreScreen();
        }
    }

    
    public void EndGame()
    {
        Image i = loadImage("sprites/story/4.png");
        
        
        for(int k = 0 ; k < 300 ; k++) 
        {
        
            
             Graphics2D g = screen.getGraphics();
            
            
            //Do Everything in this Function
            g.drawImage(i , up, up, null);
    
            
            g.dispose();
            
            screen.update();
            
            // take a nap
            try {
                Thread.sleep(30);
            }
            catch (InterruptedException ex) { }
        }
        
        
    }
    public void StoryLoop() {
        
        
        ArrayList<Image> story = new ArrayList<>();

        for(int i = 1 ; i < 4 ; i++)
        { 
            story.add(loadImage("sprites/story/"+i+".png"));
        }
        int index = 0;
        int timer = 100;
        while (running) {
        
            timer--;
            if(timer == 0)
            {
                index++;
                timer = 100;
            }
             if(index == story.size())
                 break;
            // draw and update screen
            Graphics2D g = screen.getGraphics();
            
            
            //Do Everything in this Function
            g.drawImage(story.get(index) , up, up, null);
    
            
            g.dispose();
            
            screen.update();
            
            // take a nap
            try {
                Thread.sleep(30);
            }
            catch (InterruptedException ex) { }
        }
      

    }
    
        
    
    
    //Main Game LOOP
    
    public void animationLoop() {
        
        
        for(int i = 0 ; i < Bricks.size() ; i++)
        {

            UpdateBufferMultipleStage(Bricks.get(i).getx(), Bricks.get(i).gety(), Bricks.get(i).getBufferValue(), Bricks.get(i).getHeight(), Bricks.get(i).getWidth());
        }
        
        while (running) {
        
            
            // draw and update screen
            Graphics2D g = screen.getGraphics();
            
            
            //Do Everything in this Function
            draw(g);
            
            g.dispose();
            
            screen.update();
            if(killed)
                break;
            if(changeStage)
                break;
            // take a nap
            try {
                Thread.sleep(30);
            }
            catch (InterruptedException ex) { }
        }
      

    }
    
    
    
    private void explodeBomb(int x , int y)
    {
            for(int i = 0 ; i < bombes.size() ; i++)
            {

                Bomb e = bombes.get(i);
                if( x >=e.positionx && x < e.positionx+e.getWidth() && y >= e.positiony && y < e.positiony + e.getHeight())
                {
                    e.explode();
                }
            }
        }
    
    private void UpdateBuffer(int x , int y , int value, int height , int width )
    {
        if(stage[x][y] == value)
        {
            return ;
        }
        for(int i = 0 ; i < stage.length ; i++)
        {
            for(int j = 0 ; j < stage[i].length ; j++)
            {
                if(stage[i][j] == value || stage[i][j] == -value )
                {
                    stage[i][j] =0;
                }
            }
        }
        
        for(int i = x ; i < x+width ; i++)
        {
            for(int j = y ; j < y+height ; j++)
            {
                    if(y >= resy || x >= resx)
                        break;
                    
                    if(i == x && j == y)
                    {
                        stage[i][j] = value;
                    }
                    else
                    {
                        
                       stage[i][j] = -value;

                    }
            }
        }
    }

    
    private void UpdateBufferMultipleStage(int x , int y , int value, int height , int width )
    {
        
        
        if(x+width<0)
            return;
        if(y+height<0)
            return ;
        
        try
        {
            if(stage[x][y] == value && stage[x+width][y+height] == -value )
            {
                return ;
            }
        }
        catch (Exception e)
        {
            
        }
        for(int i = x ; i < x+width ; i++)
        {
            for(int j = y ; j < y+height ; j++)
            {
                    if(y > resy || x > resx || x < 0  || y < 0)
                        break;
                    if (stage[i][j] == 1)
                        continue;
                    if(i == x && j == y)
                    {
                        stage[i][j] = value;
                    }
                    else
                    {
                        stage[i][j] = -value;
                    }
            }
        }
    }
    
    private boolean addFireToBuffer (int x , int y , int value, int height , int width )
    {
        if(x+width<0)
            return false;
        if(y+height<0)
            return false;
        if(x<0)
            return false;
        if(y<0)
            return false;
        try
        {
            if(stage[x][y] == value && stage[x+width][y+height] == -value )
            {
                return true;
            }
        }
        catch (Exception e)
        {
            
        }
        for(int i = x ; i < x+width ; i++)
        {
            for(int j = y ; j < y+height ; j++)
            {
                    if(y >= resy || x >= resx || x < 0  || y < 0)
                        return false;
                    if(i>=resx || j>= resy)
                        return false;
                    int position = stage[i][j] ;
                    
                    if( position == 2 || position == -2)
                        return false;
                    if((position == 5 || position == -5) && value != 0)
                    {
                        killEnemy(i , j);
                        
                    }
                    else
                    if((position == 3 || position == -3) && value != 0)
                    {
                        explodeBomb(i , j);
                    }
                    else
                    if((position == 1 || position == -1) && value != 0)
                    {
                        
                            killBomberMan();
                            //Reset Level if lives is more then 0
                    }
                    stage[i][j] = -value;
            }
        }
        
        stage[x][y] = value;
        return true;
    }
    
    private void killBomberMan()
    {
        if(!killed)
        {
            hero.killBomberMan();
            killed = true;
        }
    }
    private void killEnemy(int x , int y)
    {
        for(int i = 0 ; i < Enemies.size() ; i++)
        {
            Enemy e = Enemies.get(i);
            if(x>=e.positionx && x<e.positionx+e.getWidth() && y >=e.positiony && y <e.positiony+e.getHeight())
            {
                
                if(e.killMe())
                    Enemies.remove(i);
            }
        }
        if(stageNumber+1 == totalStages)
        {
            killBoss(x , y);
        }
    }
    
    private void killBoss(int x , int y)
    {
        if(x>=boss.positionx && x<boss.positionx+boss.getWidth() && y >=boss.positiony && y <boss.positiony+boss.getHeight())
            {
                
                if(boss.killMe())
                {
                    killedBoss = true;
                }
            }
        
    }
    
    
    private void UpdateBufferMultipleStageEnemy(int x , int y , int value, int height , int width )
    {
        
        
        if(x+width<0)
            return;
        if(y+height<0)
            return ;
        
        try
        {
            if(stage[x][y] == value && stage[x+width][y+height] == -value )
            {
                return ;
            }
        }
        catch (Exception e)
        {
            
        }
        for(int i = x ; i < x+width ; i++)
        {
            for(int j = y ; j < y+height ; j++)
            {
                    if(y > resy || x > resx || x < 0  || y < 0)
                        break;
                    if (stage[i][j] == 1)
                        continue;
                    if(stage[i][j] == hero.getbufferValue() ||stage[i][j] == -hero.getbufferValue() )
                    {
                        killBomberMan();
                    }
                    if(i == x && j == y)
                    {
                        stage[i][j] = value;
                    }
                    else
                    {
                       stage[i][j] = -value;
                    }
            }
        }
    }
    
    private Image getPlayerImage()
    {
        if (up != -1)
        {
            return hero.getup(up);
        }else
            if(down != -1 )
            {
                return hero.getdown(down);
            }else
                if(left != -1)
                {
                      return hero.getleft(left);
                }
                   //RIGH
                return hero.getright(right);
                   
    }
   
    private void drawBombs(Graphics2D g)
    {
        
        Image bombImage = null;
        for(int i = 0 ; i < bombes.size() ; i++)
        {
            Bomb b = bombes.get(i);
            bombImage = b.getImage();
            ArrayList<Flame> flames = null;
            if (bombImage != null)
            {
                UpdateBufferMultipleStage(b.positionx, b.positiony, b.getBufferValue(),b.getHeight() , b.getWidth());
                g.drawImage(bombImage, b.positionx , b.positiony, null);
                
            }
            
            else if ((flames = b.getFlame())!= null)
            {
                
                boolean isRemoved = false;
                for(int k = 0 ; k <flames.size() ; k++)
                {
                    Flame flam = flames.get(k);
                    Image flameImage = flam.getFlame();
                    if(flameImage == null)
                    {
                        if(!isRemoved)
                        {
                            bombes.remove(i); 
                            isRemoved = true;
                        }
                        addFireToBuffer(flam.positionx, flam.positiony, 0,flam.getHeight() ,  flam.getWidth());
                    }
                    else
                    {
                        if(addFireToBuffer(flam.positionx, flam.positiony, flam.getBufferValue(),flam.getHeight() ,  flam.getWidth()))
                            g.drawImage(flameImage, flam.positionx , flam.positiony, null);
                    }
                }
            }
            
        }
    }
    
    private void drawEnemies(Graphics2D g)
    {
        for(int i = 0 ; i < Enemies.size() ; i++)
        {
            Enemy e = Enemies.get(i);
            
            Image enemyImage = e.getImage();
            UpdateBufferMultipleStageEnemy(e.positionx, e.positiony, e.getBufferValue(),e.getHeight() , e.getWidth());
            g.drawImage(enemyImage, e.positionx, e.positiony, null);
            
        }
       // System.out.println(stageNumber+1 +"  total  "+totalStages);
        if(stageNumber+1 == totalStages)
            drawBoss(g);
    }
    
    private void drawBoss(Graphics2D g)
    {
        
        Image enemyImage = boss.getImage();
        UpdateBufferMultipleStageEnemy(boss.positionx, boss.positiony, boss.getBufferValue(),boss.getHeight() , boss.getWidth());
        g.drawImage(enemyImage, boss.positionx, boss.positiony, null);
    }
    
    public void draw(Graphics2D g) {
         
        // draw background
        //Here
        if(!isStarted)
        {
           Image i = stageStarterImages.get(stageNumber);
           startUpCounter--;
           if(startUpCounter == 0)
           {
               isStarted = true;
           }
           g.drawImage(i, 0, 0, null);
           return;
        }
        if(isPaused)
        {
            
            Image i = stageStarterImages.get(stageNumber);
            g.drawImage(i, 0, 0, null);
            g.drawImage(imagePause, 210,250, null);
            return;
        }
        
        g.drawImage(bgImage, 0, 0, null);
        
        if(killedBoss == true)
        {
            changeStage = true;
            return ;
        }   
         if(Enemies.isEmpty() && stageNumber+1 != totalStages )
        {
            
            changeStage = true;
            return ;
            /*
            g.drawString("You Won", resx/2, resy/2);
            g.drawRect(resx/2-15, resy/2-30, 80, 50);
            return ;*/
        }
            
        Image ig = getPlayerImage();
        
        UpdateBuffer(hero.positionx, hero.positiony, hero.getbufferValue(),hero.getheight() , hero.getWidth());
        
        drawBombs(g);
        //Drawing enemies
        drawEnemies(g);
        
        for(int i = 0 ; i < resx ; i++ )
        {
            for(int j = 0 ; j < resy ; j++)
            {
                int temp = stage[i][j];
                if(temp == 0)
                    continue;
                if(temp == 1)
                {
                    g.drawImage(ig, i, j, null);
                }
                else
                    if(temp == 2)
                    {
                        g.drawImage( brickImage , i , j , null);
                    }
             }
        }
    }

   
    
    private static int smartPlace(int n)
    {
        
        int [] Array = {0 , 50 , 95 , 145, 192 , 240 , 287 , 334 , 382 , 430, 477 , 525, 575};
        
        int diff1 = Integer.MAX_VALUE;
        int value = 0;
        for(int i = 0 ; i < Array.length ; i++)
        {
            int di2 = Math.abs( n - Array[i]);
            if(di2 < diff1)
            {
                value = Array[i];
                diff1 = di2;
            }
            else
                return value;
        }
        
        return 0;
    }
    
    //Main Key Press Event
    @Override
    public void keyPressed(KeyEvent ke) 
    {
        
        
        if(ke.getKeyCode() == KeyEvent.VK_P)
        {
            isPaused = !isPaused;
            return;
        }
        
        if(isPaused == true)
            return ;
        if(ke.getKeyCode() == KeyEvent.VK_K)
        {
            Enemies.clear();
        }
        if(ke.getKeyCode() == KeyEvent.VK_UP)
        {
            if(hero.positiony>0 )
            {
                
                
                        int temp = stage [hero.positionx+hero.getWidth() ] [ hero.positiony - 1];
                        int temp2 = stage [hero.positionx  ] [ hero.positiony -1 ];
                        if( temp == 2||temp == -2 || temp2 == 2 ||temp2 == -2 || temp == 3 ||temp == -3 || temp2 == 3 || temp2 == -3)
                        {

                        }
                        else
                                hero.positiony-=movementFactor;
                        
                        /*
                        if( temp == 0 && temp2 == 0 )
                        {
                            hero.positiony-=movementFactor;
                        }*/
            }
            
            up++;
            up%=imagesUsed;
            down = -1;
            left = -1;
            right = -1;
                
        }else
            if(ke.getKeyCode() == KeyEvent.VK_DOWN)
            {
                if( resy > hero.positiony + hero.getdown(0).getHeight(null))
                {
                        
                    int temp = stage [hero.positionx+hero.getWidth() ] [ hero.positiony + hero.getheight()+1];
                    int temp2 = stage [hero.positionx ] [ hero.positiony + hero.getheight()+1 ];

                    if( temp == 2||temp == -2 || temp2 == 2 ||temp2 == -2 || temp == 3 ||temp == -3 || temp2 == 3 || temp2 == -3)
                    {

                    }
                    else
                            hero.positiony+=movementFactor;

                    /*
                    if( temp == 0 && temp2 == 0 )
                    {
                        hero.positiony+=movementFactor;
                    }*/

                        
                }
                down++;
                down%=imagesUsed;
                up = -1;
                left = -1;
                right = -1;
                
            }else
               if(ke.getKeyCode() == KeyEvent.VK_LEFT)
               {
                   if(hero.positionx>0)
                   {
                        int temp = stage[hero.positionx-1][hero.positiony];
                        int temp2 = stage[hero.positionx - 1][hero.positiony+ hero.getheight()];
                        
                        if( temp == 2||temp == -2 || temp2 == 2 ||temp2 == -2 || temp == 3 ||temp == -3 || temp2 == 3 || temp2 == -3)
                                    {
                                        
                                    }
                                    else
                                         hero.positionx -= movementFactor;
                        /*
                        if( temp == 0 && temp2 == 0 )
                        { 
                            hero.positionx-=movementFactor;

                        }*/
                        
                   }
                    left++;
                    left%=imagesUsed;
                    up = -1;
                    down = -1;
                    right = -1;
               }else
                   if(ke.getKeyCode() == KeyEvent.VK_RIGHT)
                   {
                            if(resx > hero.positionx + hero.getdown(0).getWidth(null))
                            {
                                if(hero.positionx+hero.getWidth()+1<screen.getWidth())
                                {
                                    int temp = stage[hero.positionx+hero.getWidth()+1][hero.positiony];
                                    int temp2 = stage[hero.positionx+hero.getWidth()+1][hero.positiony+ hero.getheight()];
                                    if( temp == 2||temp == -2 || temp2 == 2 ||temp2 == -2 || temp == 3 ||temp == -3 || temp2 == 3 || temp2 == -3)
                                    {
                                        
                                    }
                                    else
                                         hero.positionx += movementFactor;
                                    
                                    int numBombs = bombes.size();
                                    if(numBombs >=1)
                                    {
                                        Bomb b = bombes.get(numBombs-1);
                                        if( b.positionx<hero.positionx   &&  b.positionx+b.getWidth() > hero.positionx+hero.getWidth() && (b.positiony<=hero.positiony-5 ||  b.positiony<=hero.positiony+5 ))
                                            hero.positionx += movementFactor;
                                    }
                                    
                                }
                                
                                
                            }
                            
                            right++;
                            right%=imagesUsed;
                            up = -1;
                            down = -1;
                            left = -1;
                   }
        if(ke.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
            running = false;
            System.exit(0);
        }
        if(ke.getKeyCode() == KeyEvent.VK_BACK_SPACE)
        {
            if(inStartMenu)
                menuIndex = 1;
            return ;
        }
        if(ke.getKeyCode() == KeyEvent.VK_SPACE)
        {
            
            
            if(bombes.size() < bombsAllowed)
            {
               
                int x = smartPlace(hero.positionx);
                int y = smartPlace(hero.positiony);
                
                
                if(stageNumber+1 == totalStages)
                {
                    x = hero.positionx+2;
                    y = hero.positiony;
                }else
                if(stage[x][y] == 2 || stage[x][y] == -2)
                {
                    x =smartPlace(hero.positionx+20);
                    y =smartPlace(hero.positiony+20);
                     if(stage[x][y] == 2 || stage[x][y] == -2)
                    {
                        x =hero.positionx+2;
                        y =hero.positiony;
                    }
                    
                }
                Bomb b = new Bomb(x, y , 5);
                bombes.add(b);
            }
        }
    }
    
   
   
    
    
    
      @Override
    public void keyTyped(KeyEvent ke) {
        ke.consume();
        //throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public void keyReleased(KeyEvent ke) {
           // throw new UnsupportedOperationException("Not supported yet.");
        ke.consume();
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        
      
    }

    @Override
    public void mousePressed(MouseEvent me) {
        int x = me.getX();
        int y = me.getY();
        if(!inStartMenu)
            return ;
        if(x>204 &&y>325 && x<435 && y <377)
        {
            inStartMenu = false;
            menuIndex = -1;
        }
        if(x>47 && y>402&&x<192 &&y<440)
        {
            //Instructions
            menuIndex = 2;
        }
        if(x>245 && y>402&&x<396 &&y<440)
        {
            menuIndex = 3;
                //High Score
                
        }
        if(x>449 && y>402&&x<596 &&y<440)
        {
            //Exit
            System.exit(0);
        }
        
    }

    @Override
    public void mouseReleased(MouseEvent me) {
       
    }

    @Override
    public void mouseEntered(MouseEvent me) {
       
    }

    @Override
    public void mouseExited(MouseEvent me) {
       
    }
    
}
