/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman;

import java.awt.Image;
import java.util.ArrayList;

/**
 *
 * @author Malik Ali
 */
public class Bomb 
{
    ArrayList<Image> bombImages = new ArrayList<>();
    public  int positionx;
    public  int positiony;
    private int height;
    private int width;
    private boolean isActive = true;
    private int BufferValue = 3 ;
    private ArrayList<Flame> Flames = new ArrayList<>();
    private int bombIndex = 0;
    private int bombTimer = 45;
    public Bomb(int x , int y , int numberOFImages)
    {
        for(int i = 1 ; i <= numberOFImages ; i++)
        {
            bombImages.add( MainClass.loadImage( "sprites/bomb/"+i+".png" ) );
            
        }
        positionx = x ;
        positiony = y;
        
        height = bombImages.get(0).getHeight(null);
        width = bombImages.get(0).getWidth(null);
        
        
        Flame center = new Flame("sprites/flame/center/", 4);
        center.positionx = positionx;
        center.positiony = positiony;
        Flames.add(center);
        
        Flame right = new Flame("sprites/flame/right/", 4);
        right.positionx = positionx+center.getWidth();
        right.positiony = positiony;
        Flames.add(right);
        
        Flame left = new Flame("sprites/flame/left/", 4);
        left.positionx = positionx-left.getWidth();
        left.positiony = positiony;
        Flames.add(left);
        
        Flame down = new Flame("sprites/flame/down/", 4);
        down.positionx = positionx;
        down.positiony = positiony+getHeight();
        Flames.add(down);
        Flame up = new Flame("sprites/flame/up/", 4);
        up.positionx = positionx;
        up.positiony = positiony - up.getHeight();
        Flames.add(up);
        
        
    }
    public int getHeight()
    {
        return height;
    }
    public int getWidth()
    {
        return width;
    }
    
    public int getBufferValue()
    {
          return BufferValue;
    }
    public Image getImage()
    {
        if(!isActive)
            return null;
        bombTimer--;
        if(bombTimer <= 0)
        {
            bombIndex++;
            bombTimer = 45-bombIndex*3;
        }
        if(bombIndex >= bombImages.size())
        {
            SoundPlayerWav sp = new SoundPlayerWav("sounds/boom.wav");
            return null;
        }
        return bombImages.get(bombIndex);
    }
    public ArrayList<Flame> getFlame()
    {
        return Flames;
    }
    public void explode()
    {
        isActive = false;
    }
}
